---
layout: single
classes: wide
title: About
permalink: en/about
ref: about
author_profile: true
header:
  overlay_image: assets/images/headers/main-header.jpg
  image_description: "Helsinki Cathedral"
---

Hi, I'm Christoph, a Computer Science student from the south of Germany that is now living in Helsinki, Finland.
When moving to Finland I noticed that there will probabily be a lot on my mind and to document the process of starting a new chapter of my life and to structure the thoughts in my head I came up with the idea of writing a blog about it.

However, I do not want to restrict this blog to be only about the topic of moving to a new country.
I do think about a lot of different stuff and want to keep the freedom to write about all of that on this blog.
This is also where the name came from, you can expect a _broad spectrum_ of topics that the posts will be about.

Now a bit more about me.
As already mentioned, I am from the south west of Germany and after I finished my Bachelors degree in Mechatronics there, I decided that I wanted to try something new for my Masters degree.
Then the idea started to move to northern Europe to study there.
Long story short, I ended up with a study place at the University of Helsinki in the Computer Science Master programme.

Besides my studies, I am interested in music, a bit of photography, climbing and I am a christian, so you can probably expect posts from most of these areas at some point.
If you have any questions, feel free to contact me.