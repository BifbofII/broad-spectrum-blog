---
title: "Seizing Opportunities"
ref: seizing-opportunities
category: personal
verse: So then, as we have opportunity, let us do good to everyone, and especially to those who are of the household of faith. - **Galatians 6:10**
header:
  overlay_image: assets/images/headers/opportunity.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: "[Picture of an open door](https://unsplash.com)"
---
Something that I have been trying to do a bit more consciously lately is how I deal with opportunities that come up.
Previously, if an opportunity arose, I based my decision on what to do with that possibility on my gut feeling.
That meant that if I was currently tired, it was unlikely that I would agree to make plans, even if it was for the next day.

# The Problem
In my current situation where I am in a city that is still new to me and I do not know many people, that behaviour could lead to me spending most of my time at home.
The fact that all my courses are held online does not necessarily help in breaking the habit of self-induced isolation.

Meeting new people in this situation is harder than it would normally be when starting your studies at a new university.
In the big online lectures, you see no more of the other students than a name, written on your screen.
Even if there are people that take the same courses as you, it is difficult finding them, because you do not bump into each other in the class rooms.
Sure, there are group chats and smaller exercise groups where you can talk to other students and practice in small groups, but this is definitely not the same as it would be under normal circumstances.

# The Decision
The question now is, how I want to deal with this situation.
Do I say to myself: "Well, the situation is going to change again, I can still meet more people next term, when classes will start to be at campus again." or do I decide to do something about it and make the best out of it?

What I decided was to increase my chances of meeting interesting people by accepting almost all opportunities that come to me.
I was already attending most of the meetups for new students during the orientation week, I try to find the few events that are organized in person that interest me at least a bit and see if I meet people there.
Additionally, I attend online study groups, to get to know people that are in the same courses as me, I have already been to one church, and I am planning to go to another one soon, just to name a few examples.

After trying this generally accepting frame of mind for a while now, I have already had quite a few experiences that have positively surprised me.
I would describe myself as more of an introverted person, so going somewhere where I do not know anyone and getting to know new people is challenging.
But from my limited experience, I must say that the fear of bad experiences is quite irrational.
Not all events that I went to where a smashing success of course, but people generally are friendly to newcomers, especially at events in the beginning of the academic year and as I have already mentioned, some situations resulted in pleasant surprises.

The great thing about this approach is that the more opportunities you take, the more opportunities you will get.
Even if the first few are not exactly what you hoped for, maybe they will lead to other opportunities that are what you expected.

# Opportunities Given to Us
One last thought on that topic.
I think that opportunities do not only come to us by pure luck, but that they are given to us.
As a Christian, I think that God provides and wants the best for us.
And therefore, I am sure, he provides us with opportunities, the only thing that we need to do is to seize them.
There are many stories in the bible where God provides for his people, but usually they still need to take what opportunities are given to them.
Take the story of Nehemia as an example.
As the cupbearer of the king, God had provided him with the opportunity to ask the king to allow him to rebuild the wall of Jerusalem, but he still needed to seize the opportunity and talk to the king.

To not miss out on good opportunities that are given to me, I want to be more mindful of what decisions I take.