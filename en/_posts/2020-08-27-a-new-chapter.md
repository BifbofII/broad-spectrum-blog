---
title:  "A New Chapter"
ref: new-chapter
category: personal
verse: For everything there is a season, and a time for every matter under heaven. --- **Ecclesiastes 3:1**
header:
  overlay_image: assets/images/headers/chapter.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: An open book
---
Sometimes in life, a big change occurs.
For example when one is moving, starting to study at university or starting a new job.
I like to think of those changes as new chapters in my life.
Each of them is sort of self-contained and when a new chapter starts, some old stuff is left behind and new things begin.
In addition to that, there might be different levels of chapters (chapters, sections, subsections, ...) that represent different levels of change in our lives.

Recently, I started a new chapter in my life.
And it may be the biggest new chapter that I have ever started.
After I finished my Bachelors degree in February this year, I decided that I wanted to do something a bit different for my Masters studies, which lead to me now studying at the University of Helsinki, 1600 km away from my home in the south of Germany.

# All New
If the chapters in my life had titles, the last year maybe would be called _The Transition_.
In that time, I transitioned my studies from Mechatronics in my Bachelors degree to Computer Science, which I wanted to do in the future.
Additionally, there were many things to prepare for the big move.

And now the chapter _All New_ is starting.
I've relocated to Finland now and the semester is ramping up.
This chapter switch is an especially big one for me, because most aspects of my life are changing.
Up until now, I studied close to home, which meant I was still living at my parents'.
Furthermore, I have never been living abroad or somewhere where I did not know the main language and I have never moved at all.
Regarding my studies, I did my Bachelors degree at a small-ish University of Applied Sciences and in Mechatronics.
Now I am at one of the worlds top 100 universities, according to some rankings and studying Computer Science.

All these new things are going to define the new chapter in my life, and I am curious to see what experiences I am going to have in the two years that I will be staying here.

But with all the new, there are also things that I need to leave behind.
Since I lived in the same place, a rather small village, all my life, I do only now realize how important some surrounding things were for me.
It is odd walking through the city and not knowing anybody at all.
Even though I still have contact to my friends at home, they are not around and I can't meet them.
I do not have the church that I was going to, or the YMCA that I was volunteering at.
And so the list goes on and slowly grows as my feeling changes from just being on holiday to realizing that I actually live here now.

# But Why?
So the question that poses itself and that I was already asked quite a few times since I am here now is "Why?".
Why did I decide to study abroad, it's not like German universities are bad?
And why Finland?

Well, I was interested in the nature of Fennoscandia (as I recently learned, Scandinavia does not include the southern part of Finland) for quite some time and was looking for an opportunity to get to know the area.
Therefore, I applied to different universities in Norway, Sweden and Finland, but only got accepted at the University of Helsinki.
If I had had the choice, I do not think I would have decided to go to Finland, but mostly because I did not really know anything about it.
Now that I am here and that I read up some stuff about it, I am quite interested in getting to know this country up north a bit better.

Other than that, I think the experience of living in a different country can be a valuable one.
And there is no easier time to do that than while studying.
In principle, it is also possible when working but it comes with additional hindrances that are not present as a student.

# About This Blog
I decided to start this blog to have the opportunity to write down some thoughts that are in my mind.
It probably will not be solely focused on my experience living here in Helsinki but sooner or later, other topics will come up as well.
Some more information can be found on the [About page](/en/about).