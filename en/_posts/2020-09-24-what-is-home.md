---
title:  "What Is Home?"
ref: what-is-home
category: personal
verse: For here we have no lasting city, but we seek the city that is to come. --- **Hebrews 13:14**
header:
  overlay_image: assets/images/headers/home.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: A home
---
I think it is pretty clear why I recently started thinking about the questing what _home_ is.
Moving puts you in a position where you are caught in the middle between your old place of living and your new one.
In the first week or two, here in my new room, I felt like I was on holiday.
Everything felt temporary, that might have been not in the least because I was still waiting for my furniture to arrive.
The flat here in Helsinki and the entire city were still far from feeling like home to me.

Now, after I have been here for a little more than a month, I start to feel comfortable.
I have my furniture and there is food in the kitchen, I know my way around the city a bit and do not have to buy every second thing that I need in day to day life.
But does it feel like home to me?
I don't know.

# Some Definitions of Home
According to the Oxford Dictionary, home is the following:

> The place where one lives permanently, especially as a member of a family or household.

Well then, I do permanently live in Helsinki, but my family is in Germany.
Then again, on my German ID card I have a sticker that tells me that I do not have a place of living in Germany.
But of course, this definition does not capture what we mean most often when we talk about _home_.

Another common definition defines home as a _familiar or usual setting_.
This is much closer to what I am thinking about when I say that I do not know if Helsinki is my home yet.
It definitely starts to feel familiar, but not to the same extend as Germany would.

The second definition is pretty similar to something that I took from the TED talk [_Where is home?_ by Pico Iyer](https://www.ted.com/talks/pico_iyer_where_is_home).

> Home, we know, is not just the place where you happen to be born.
> It's the place where you become yourself.

In the beginning of his talk, he gives a short overview of his cultural background, which involves way more countries than mine.
And still he describes his background as 'old-fashioned and straightforward'.
So what are my thoughts on the question what home is, with an even more straightforward background?

# What Home Is to Me
Up until recently, it was very easy for me to say what my home was.
I had not moved a single time before I came to Helsinki, so home had always been in the south-west of Germany.
All my family is there, most of my friends are still there and I have not known much else.

Now that I am here, I would still consider Germany to be my home.
Even more, now that I left it, I look differently at some things that I took for granted there and realize how important they were for me.
In that sense, leaving home can even strengthen ones bond to it.

Regarding the question if Helsinki is going to become a home for me in the two years that I will be living here: maybe I will come back to that question in a few months time, at the moment I can't really say too much about that.
What I can say does already feel a little like home here is the church that I found.
From the moment that I first walked in, I felt welcome and there were people talking to me.
It is comforting having people around that share the same faith and that I therefore have a connection to.

I want to wrap this post up with another thought that I took from the above mentioned TED talk:

> Where you come from now is much less important than where you're going.

In the context of the talk, the idea is that your origin does not form your home, but that what you strive for is actually way more important in defining who you and your home are.
This reminds me of Hebrews 13:14 where the author of the letter expresses that what we have here on this earth is not what will stay with us.
What defines us as Christians is what will come afterwards.
This is our true home.