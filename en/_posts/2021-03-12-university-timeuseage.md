---
title: "Expenditure of Time at University"
ref: timeuseage-university
category: data-science
verse: For everything there is a season, and a time for every matter under heaven. --- **Ecclesiastes 3:1**
header:
  overlay_image: assets/images/headers/time.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: A clock
---
I like data and numbers.
That might seem a bit weird to some, but having precise numerical measurement regarding whatever, is kind of satisfying to me.
This is one of the reasons why I started tracking the time I spend on some tasks in my life with a service called [Toggl Track](https://toggl.com/track/).
With this service, you can run named timers whenever you are working on a specific task and afterwards look at the summarized data or download it to do some more detailed analysis.
At the end of 2018 I started tracking times for different areas and one of the areas of my life where I've been the most consistent in keeping track of almost everything has been my time spent on different university lectures.
Up until now I have about 4.5 semesters worth of tracked time expenditure for lectures at three different universities and I finally decided to have a more detailed look at how much time I spend on lectures.

# About the ECTS
The [ECTS](https://en.wikipedia.org/wiki/European_Credit_Transfer_and_Accumulation_System) or European Credit Transfer and Accumulation System tries to standardize how lectures at universities all over Europe are weighted.
For this, the unit of an ECTS credit is introduced and each lecture is worth a certain amount of these credits.
What all countries that use the ECTS have in common is that the target number of credits that a student should achieve per year is 60.
What is not the same in every country is what number of working hours a single credit is taken to be equivalent as.
This number ranges from 25 to 30 hours per credit, depending on the country or even the institution within the country.

The idea behind creating a system that enables comparability between academic achievements of different countries is definitely a good one, otherwise I would have had many more difficulties starting my studies here in Finland.
However, the value assignments, especially the assignment of hours per credit point, in the ECTS seem quite arbitrary.
The vast majority of students that I have talked to about this topic have told me that they, similarly to me, would guess that they spend significantly less time per lecture compared to what the number of credits officially require.
To verify this gut feeling, I decided to have a look at my recorded data and see if I could find anything interesting.

# The Data and the Process
As mentioned above, I tracked my time with Toggl.
From their website, a CSV file can be downloaded, which, for each time entry, contains the given timer name (in my case the name of the lecture), the start time and the end time.
After downloading the data, I processed the data in a Jupyter Notebook, which can be found [here](/special-pages/university-timelogs-nb) for whomever is interested, cleaned up the data and summed up all the durations of the single time entries per lecture.
In addition to this data, I manually entered how many credits each of the lectures was worth and at which university I had taken that lecture.
To convert from the credits to required hours, I decided to use 25 hours per credit, since that was the lowest value for Germany (25-30) and Finland (27), where I've studied.
By putting that information together, I could then compute, what I call the time fraction $\lambda=\frac{T_\text{recorded}}{T_\text{ECTS}}$.
This time fraction $\lambda$ will give a fractional value for each lecture which tells us how much of the required time I have actually expended. The interpretation hereby is the following:

| $\lambda$ | Interpretation                              |
| --------- | ------------------------------------------- |
| $1$       | I've spend exactly as much time as required |
| $1.5$     | I've spend 50% more time as required        |
| $0.5$     | I've spend halve as much time as required   |

# The Findings
After this processing, I could finally start having a look at how much time I have spent per lecture.
The first measure which I had a look at was the average time fraction $\lambda$ over all lectures.
This turned out to be $0.387$ meaning that on average, I have spent less than 40% of the required time per lecture.
Another idea, which I wanted to look into, was to see if I could find any differences in the data, between the three universities that I've been to.
These universities are Reutlingen University, Eberhard Karls University in Tübingen and the University of Helsinki.
Therefore, the next thing which I checked was to compare the averages for the different universities.
Here I got the following results:

| University | $\bar{\lambda}$ |
| ---------- | --------------- |
| Reutlingen | $0.400$         |
| Tübingen   | $0.481$         |
| Helsinki   | $0.340$         |

I had expected that my time expenditure in Tübingen would be higher than for the other Universities, what surprised me was that the fraction for Helsinki was that much lower than for Reutlingen.
However, as I have learned in the book [Factfulness]({% post_url en/2020-10-24-factfulness %}), averages can deceive and therefore I continued with a more graphical analysis of the data.
For this I used the [Plotly Library](https://plotly.com/python/) to generate an interactive graph in which I could further explore the data.

The following graph, which I finally arrived at, might require some explanation:
This type of graph is called a [box plot](https://en.wikipedia.org/wiki/Box_plot) with the difference to most box plots being that each individual data point is additionally plotted as a marker.
With a box plot, the distribution of a group of data points can be plotted.
This includes the maximum and minimum values, disregarding any outliers (represented through the top and bottom horizontal line), the main box represents the range where the middle 50% of all data points lie and the line in the middle is the median of the data.
With this way of plotting, a lot of information about the group of points can be summarized in a relatively simple way.
Additionally, I have decided to plot all data points.
This allows to interactively look at a single lecture, get the exact values and also potentially find explanations for outliers.

{% include specials/university_timelogs_plot.html %}

I first had created this plot without including the colour category showing if a course is an online course or not.
When looking at this data, I noticed that three lectures I took at Helsinki University had significantly lower values than the others.
On further investigation I noticed that exactly these three courses were online courses which were entirely done in self-study based on online materials.
This is why I decided to separate those courses out into their own group to not skew the data for 'normal' lectures I took here in Helsinki.
These three lectures do also explain why the average value for Helsinki was so surprisingly low.

Some interesting things that I did notice in this plot:
* Only a single course does even remotely come close to me having spent as much time on the course as I should have.
  This was my Bachelor's thesis which I wrote in Reutlingen.
* The spread of the data for Reutlingen is quite high, suggesting that the lectures there are unequal in terms of how much time they require.
* Online courses seem to be a time-efficient way of gaining credits.
* My average time spent on courses in Tübingen was the highest, but I also only was there for one semester and only took three courses providing not as much data as for the other Universities.

# What Now?
Well, what can we do with these findings now?
To be honest, I don't know.
One thing which I realized was that a gut feeling about how much time you spend on something is not necessarily a good indicator.
The data shows that I have spent more time on the lectures in Tübingen, however I would have thought that the difference was more pronounced.
The other gut feeling, that I do generally spend not as much time on lectures as the number of credits would suggest is verified though.

One final note on the data that I recorded and how much that can tell us.
Obviously, this is only my personal experience.
It would be very interesting to compare these findings to data from other students to see how this compares to other students study lives.
In addition, even though I would say that I am thorough with tracking time when I am productively working on something, I do realize that I get ideas about how an assignment can be solved or how to continue with a tricky task that I am stuck at while taking breaks.
These times are obviously not tracked.
Therefore, the found time fractions are maybe more like a lower bound on the time that I've actually spent on lectures but still I would say that the 25 hours per credit do not really apply to how I am working for university.