---
title:  "How Has Home Changed?"
ref: how-has-home-changed
category: personal
verse: Unless the Lord builds the house, those who build it labour in vain. Unless the Lord watches over the city, the watchman stays awake in vain. --- **Psalm 127:1**
header:
  overlay_image: assets/images/headers/home2.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: A house
---
For the last three weeks I was back in Germany.
Being back has brought up the question of home again, or more precisely how home has changed.
In my situation, this question contains two different levels:
First, what has changed back home, meaning in Germany, in the year that I haven't been there.
Secondly, how has the definition of home changed for me.
I will try to lay out some thoughts on these questions, with the second part revisiting some thoughts I have already brought up in this [post from last September]({% post_url en/2020-09-24-what-is-home %}).

Quite honestly, not that much has changed in a year.
This is probably true for most years, but especially for a year in which a global pandemic inhibits most creative ideas to start something new.
So going back to Germany, almost everything felt very familiar.
I've met a lot of people again that I do know well but haven't spoken to in a long time and visited a lot of places that I still know well.
It was also very nice to see some projects that I was working on in the church or the YMCA continued by others.
So what has changed the most?
Well, probably me.

Even though all the things back home do still feel familiar to me, I do not live there any more and I haven't for almost a year at this point.
In my post on _what home is_, back in September, I still didn't know if Helsinki was going to become a home for me.
At that point I had only been here for about a month, still had no idea of many of the amazing people I would be meeting here and how comfortable I would become.
Since then a lot has changed, even if I didn't notice it that much in the process.
Going back to Germany triggered my brain to think about this topic again, and now I see a bit clearer how much has changed.

The entire time I was in Germany, I felt like I was only there as a visitor.
I guess this makes sense, since this is what I was there as, however, in advance I wasn't so sure that it would feel that way.
It could have very well felt a lot more like coming back permanently and then having to tear myself out again at the end.
But I am quite happy that this was not the case.
Feeling as a visitor showed me that I have accepted Helsinki as my home for now.
I am glad that this change has happened since September since otherwise the upcoming year here would probably be a lot more challenging.

So how can I summarize all of this?
I still don't quite know what I would use as a definition of home, but for now I can say that Helsinki _is my_ home.
Being back in Germany and meeting some people in person again was obviously nice as well, but taking the bus from the train station to my place yesterday did definitely contain this relieving feeling of coming home.
I'm glad for all the people from very diverse backgrounds that I could meet here.
In this year, despite Covid, I could meet around 80 people from more than 20 different nationalities.
This is an amazing gift, and I'm very thankful for that.

I'm looking forward to going into this next year here in Helsinki and finding out what God has in store for me here.