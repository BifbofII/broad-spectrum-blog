---
title:  "Some Impressions of Finland"
ref: impressions
category: finland
verse: And he is before all things, and in him all things hold together. --- **Colossians 1:17**
header:
  overlay_image: assets/images/headers/lake.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: A lake
---
After I have been here in Finland for almost two months now, I wanted to compile a few impressions that stood out to me about the country, its culture and the language.
I hope that some of these things are interesting to those of you who, like I did before coming here, do not know that much about Finland.

# Language
Probably one of the more well known facts about Finland is that the Finnish language is quite special and not really comparable to most other languages.
According to [Wikipedia](https://en.wikipedia.org/wiki/Finnish_language), Finnish belongs to the Uralic languages.
In addition to Finnish, only Hungarian and a few minority languages (including Sámi, which is spoken by Sámi people in northern Finland) belong to this family of languages.
This explains why the language is so different from most languages we know, which belong to the Indo-European language family.

The first obvious thing to notice is that most words are not similar to any English, German or French translations, which makes the vocabulary hard to acquire.
An example of that could be the word sun (_de_: Sonne, _fr_: soleil) which is aurinko in Finnish.
But on further investigation it becomes clear that not only the vocabulary but also the grammar is quite different.
Things like that the negation word 'not' is actually more like a verb in Finnish and needs to be conjugated or that the word 'with' is a grammatical construction based on the genitive case and a word added to the end are initially hard to grasp.

What is quite helpful in day-to-day life is that Finland is officially a bilingual country.
Swedish is the second official language, which means that most signs are in Finnish and Swedish.
Therefore, (as a native German speaker) the meaning of written information can sometimes be guessed from the Swedish translation, which is much closer to German or English.
But luckily most Finns speaks English very well so that it is not a big problem to ask somebody for help.

# Education System
Word wide, the Finnish education system is considered to be one of the best.
Finland regularly scores one of the highest ranks in the [Programme for International Student Assessment (PISA)](https://en.wikipedia.org/wiki/Programme_for_International_Student_Assessment) and the [Human Development Index](https://en.wikipedia.org/wiki/Human_Development_Index) of the country is also one of the best, which is attributed to the national education system.

I can personally only give a few insights on what I have noticed in my first period at the University of Helsinki.
In contrast to German universities, I would say that there is more immediate contact to the professors in Finland.
I do not know any official numbers on how the student to professor ratio compares between Germany and Finland, but I would guess that the ratio at a Finnish university is closer to that of a German University of Applied Sciences than a German university.

Additionally, the communication between students and university staff feels more relaxed here.
I have noticed that also outside of university in settings where in Germany I would assume a quite formal communication (e.g. from authorities), but it is very apparent at university.
Everybody, from students to professors, goes by their first names, I have notices teachers joking around with students in group chats and have seen an impressive number of [memes used in official lecture material](/assets/images/gdpr_meme.png).

# The City of Helsinki
I have never lived in any other city before, so I am not really able to compare my impressions directly to somewhere else, but here is what I noticed:

There is a lot of nature integrated into the city.
Every few blocks you can find a new park with at least a few trees and benches to sit down and take a break.
Probably the most impressive park is [Helsinki Central Park](https://www.myhelsinki.fi/en/see-and-do/nature/central-park-the-central-forest-running-the-length-of-helsinki) which runs all the way across Helsinki, almost from the southern coast to the northern border, making it possible to cross Helsinki almost entirely in nature.

It is a nice city to get around by bike.
There are many gravel paths through the parks or paved bike lanes along the bigger roads.
Most places in the city are very easy to reach by bike and there are also city bikes available to cheaply rent from bike stations.
Only in the city centre it can get a little uncomfortable by bike, since many roads and walkways there are made from cobblestones.

There are also a huge number of construction sites all over the city.
Building new tram lines, new bike lanes or just renewing the road surface, it does not seem like the city is skimping on spending money for public roads.

# Nature
Finland is almost [three quarters covered in forests](http://www.borealforest.org/world/world_finland.htm).
Because of that not only the first forests but even national parks can be easily reached from Helsinki by public transport.
Going for a hike in [Nuuksio](https://www.nationalparks.fi/nuuksionp) or one of the other national parks provides a great contrast to everyday city life.

The second defining feature of Finland's nature is definitely lakes.
Finland has [more than 180 thousand lakes](https://en.wikipedia.org/wiki/List_of_lakes_of_Finland), not even counting the smallest ones.
When hiking in the forest great picnic spots next to a lake are therefore quite easy to find.

A last thing that I have noticed is the plethora of glacier formed structures.
Smoothly rounded Granit rocks, skerries and glacial erratics can be found everywhere.

# Summary
These were just a few things that I have noticed in my limited time that I have been living here now.
They are obviously not a comprehensive summary of Finland but only based on my experience.
I hope that this post could still give you an impression of what it is like to live here.
For some more visual impressions of mostly Helsinki, feel free to check out the [Gallery](/en/gallery).