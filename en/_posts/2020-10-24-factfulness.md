---
title:  "Factfulness"
ref: factfulness
category: data-science
verse: Trust in the Lord with all your heart, and do not lean on your own understanding. --- **Proverbs 3:5**
header:
  overlay_image: assets/images/headers/data.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Graphs of data
---
Here is the first post that is not about me or Finland.
I recently read the book _Factfulness_ by Hans Rosling and here is a short summary and some thoughts on it.

# Summary
[Hans Rosling](https://en.wikipedia.org/wiki/Hans_Rosling) was professor for international health in Sweden, but what he is most famous for is not his medical work.
It rather is his view on and how to properly work with data.

He held many talks (like this [TED talk](https://www.youtube.com/watch?v=hVimVzgtD6w)) in which he famously uses animated bubble charts to show development over time and although many of these talks are on global health topics, they do not gain their popularity from new medical discoveries.
The data shown in the talks has usually been publicly available for years, the difference is how this data is presented.

In his book, Hans Rosling focuses on why our world view is often wrong, even tough there is data that we could look at to know, that we are wrong.
He introduces a new way of looking at the world, which he calls _factfulness_.
This, he says, is highly against how we humans instinctually work.
But it is also necessary to have a correct world view to be able to make proper decisions.

Throughout the course of the book, he highlights many global topics about which most people do not know enough, to answer simple fact questions.
An example for such a question is:

> In the last 20 years, the proportion of the world population living in extreme poverty has...
> * A: almost doubled.
> * B: remained more or less the same.
> * C: almost halved.

The book contains multiple such questions, and over and over the presented results show that the majority does not only not know the answer, but most people think they know the answer but are wrong.
As the book puts it: even a chimpanzee that picks the answers randomly would score better than most humans.
(Btw, answer C is correct in the above example.)

In an attempt to explain why we often perceive our world so inherently wrong, Hans Rosling defines 10 dramatic instincts which we follow when we are presented with a question, that lead us to wrong assumptions.
By knowing these instincts, we can counteract them to achieve a more factful world view.

The ten instincts are:
* The _Gap_ Instinct: We often assume that things are divided into two clear-cut groups that are separated by a gap.
However, this is not the case most of the time.
Usually most people, countries or whatever we are talking about, can be found in the middle, exactly where we assume the gap to be.
* The _Negativity_ Instinct: We often get a negative view of the world because only the negative news reach us.
Positive things are often not interesting enough to be reported.
In addition to that, things can be bad but at the same time improving.
* The _Straight Line_ Instinct: If we see a trend in data, we usually assume that this trend will continue in a straight line.
* The _Fear_ Instinct: If something scares us, we tend to overestimate the risk that actually comes from that thing.
Risk is a factor of both, danger and exposure, but the second part is often forgotten.
* The _Size_ Instinct: When seeing a single number, it is usually hard to judge its actual size without comparing it to put it into perspective.
* The _Generalization_ Instinct: We like to group things into categories to make them easier to understand.
This is useful, however, we need to pay attention if we chose our categories wisely.
* The _Destiny_ Instinct: Change is often so small that we can't perceive it.
Therefore, we often assume that things are constant and will never change.
* The _Single Perspective_ Instinct: A single perspective can often limit our imagination and lead us to the wrong conclusions.
* The _Blame_ Instinct: We usually look for villains instead of causes and heroes instead of systems that improved things.
* The _Urgency_ Instinct: Most of the time, we assume that problems are more urgent than they actually are.
Because of this it is easy to make rushed and not fact-based decisions.

If these points sound interesting, I really recommend reading the book for more details.

# Personal Thoughts
Even though I would say that I like to look up facts and reference data if it is needed, I did do quite badly on the questions in the book.
So I definitely struggle with these instincts distorting my view on the world.

However, the points he makes in the book sound really simple.
It all makes sense when it is pointed out to you.
But actually applying it is a different story.
He did not call those things _instincts_ by accident.
An instinct is something that we apply without us noticing.
Exactly that is what happens if we do not pay attention.
If we do not make an active effort to look at the actual facts, we continue in our distorted world view.

After reading the book, I am now definitely interested in trying to apply the things he pointed out.
I am interested to find out what is actually going to change.
Hopefully, I keep factfulness in mind and do not fall victim to the dramatic instincts as often.