---
title:  "A General Life Update"
ref: life-update
category: personal
verse: Have I not commanded you? Be strong and courageous. Do not be afraid; do not be discouraged, for the Lord your God will be with you wherever you go. --- **Joshua 1:9**
header:
  overlay_image: assets/images/headers/lake_boat.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: A boat on a lake
---
It has been a long while since I posted anything here, so let me finally take the time to write up a general life update.

The last time I wrote anything here was almost a year ago, after I got back from Germany.
A lot has changed since then, both personally for me and in my work and studies.
For me personally, the biggest change is definitely that I am in a relationship now, but that is not what this post shall be about.
Here I'll focus on updating everyone on the progress of my studies and plans for the future.

Almost exactly a year ago now, in May 2021, I started a summer internship in one of the research groups at University of Helsinki.
Summer research internships are a common way for Master's students here to spend their summer break, learn more, dip their toes into research for the first time, and earn some money as well.
I had decided to do this internship with the intention of answering whether I could see myself doing research work in the future or not.
So I started this internship which originally was supposed to last until the end of August.
During the internship I got to work closely together with my three supervisors, two post-doctoral researchers and our professor, originally working on finding a more efficient way of learning interpretable representations of data.
Towards the end of the three months internship our focus had slightly shifted, and we had developed a general algorithm for optimization with two conflicting objective functions.

I have learned a lot during this internship, especially during the phase of the last couple of weeks.
My supervisors decided that it was worth writing up our work as a research article and submitting it to an international conference, which was a completely new experience for me.
Maybe I'll write another post about how the scientific publication process works soon, but for now just a small summary:
If you want people to know about your research, you'll have to publish it as an article either at a scientific conference or in a journal.
For that, you submit your manuscript to be reviewed by other people in the same field of research as you and if they deem it good and interesting enough, it gets accepted and published.
Long story short, our article was _not_ deemed good enough, so it got rejected, but we got valuable feedback on how to improve it.
Not just from the reviews but even more from writing together with my supervisors, I have learned a lot in this entire process.
I have seen how strategic you need to be in writing so that your work has a higher chance of being accepted, how some published articles really still have a lot of room to be improved, and how versatile research work can be.
Overall, I've grown to love research and for this reason, I decided to stay in the research group as a research assistant.

From the summer until the end of the year, I have been working part-time while still taking university courses and since the beginning of this year, I've worked full-time in our research group.
The work I continued doing was improving what we had done during my internship and soon starting to write it up again as a new article for a different conference.
And since Easter we know that the work was worth it, our article got accepted and will be published in August at the [SAT conference 2022](http://satisfiability.org/SAT22/).
Furthermore, for the past two months I have been writing a somewhat extended version of said paper as my Master's thesis, which I also will be handing in for grading early next week.
So progress has been made, I will be graduating from my Master's studies in the next month and I will publish my first first-author paper this summer as well.
I am very glad about all of these great opportunities that came out of the research internship last summer and I would recommend doing something like this to everybody in a similar situation.

Possibly the greatest development that has come out of the summer internship is my plans for the future.
I have decided that I want to continue my path in research, which means the next step is doing a PhD.
For this, I will continue with my current supervisors and in my current field of study.
Which also means that I will be staying here in Helsinki for about the next four years.
When moving here, I had the thought in my mind that "even if I don't like it so much, two years isn't _too_ long".
Well, turns out I _do_ like it here, and I am willing to stay another four years.
There are still some details to figure out about the PhD, but basically, my current contract as a research assistant ends at the end of August and after that I'll officially start the PhD.
I'll have to write some applications to hopefully get funding directly, but even if not, I can start working with my supervisor's project funding.
Maybe I should try to write up an accessible version of my research plan (once that exists) as a blog post, but I'll not promise anything.

As a last point, let me write a few lines about my (travel) plans for the summer.
I will be in Germany for a brief time at the end of July, but the bigger two things happen after that.
Straight from Germany I will be flying to Israel, to attend the conference that our paper will be published at, and even hold a short presentation summarizing its content.
I am very excited about this opportunity to attend my first scientific conference, especially since the conference is part of a [large joint conference](https://www.floc2022.org/), meaning that there should be amazing opportunities to hear talks of impactful people and meet people whose work I've read.
This is probably another opportunity to process what I'll experience in another blog post, so let's hope I actually take more time writing in the next months.
After Israel and a brief stop here in Helsinki, I will be in Indonesia for the remainder of August.
That trip should also be full of impressions and experiences, so if I'm not posting anything here, feel free to ask me directly or nudge me to write up a blog post so that I don't create another one-year gap.