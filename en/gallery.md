---
layout: splash
author_profile: true
title: Gallery
ref: gallery
permalink: en/gallery
entries_layout: grid
header:
  overlay_image: assets/images/headers/main-header.jpg
  image_description: "Helsinki Cathedral"
excerpt: "Images from my Instagram"
gallery-001:
  - url: https://jabsserver.net/gallery/001/2008_Helsinki_016.jpg
    image_path: assets/images/gallery/001/2008_Helsinki_016-th.jpg
  - url: https://jabsserver.net/gallery/001/2008_Helsinki_022.jpg
    image_path: assets/images/gallery/001/2008_Helsinki_022-th.jpg
  - url: https://www.jabsserver.net/gallery/001/2008_Helsinki_026.jpg
    image_path: assets/images/gallery/001/2008_Helsinki_026-th.jpg
gallery-002:
  - url: https://www.jabsserver.net/gallery/002/2008_Helsinki_033.jpg
    image_path: assets/images/gallery/002/2008_Helsinki_033-th.jpg
  - url: https://www.jabsserver.net/gallery/002/2008_Helsinki_034.jpg
    image_path: assets/images/gallery/002/2008_Helsinki_034-th.jpg
  - url: https://www.jabsserver.net/gallery/002/2008_Helsinki_040.jpg
    image_path: assets/images/gallery/002/2008_Helsinki_040-th.jpg
  - url: https://www.jabsserver.net/gallery/002/2008_Helsinki_051.jpg
    image_path: assets/images/gallery/002/2008_Helsinki_051-th.jpg
gallery-003:
  - url: https://www.jabsserver.net/gallery/003/2008_Helsinki_011.jpg
    image_path: assets/images/gallery/003/2008_Helsinki_011-th.jpg
  - url: https://www.jabsserver.net/gallery/003/2008_Helsinki_013.jpg
    image_path: assets/images/gallery/003/2008_Helsinki_013-th.jpg
  - url: https://www.jabsserver.net/gallery/003/2008_Helsinki_028.jpg
    image_path: assets/images/gallery/003/2008_Helsinki_028-th.jpg
  - url: https://www.jabsserver.net/gallery/003/2008_Helsinki_048.jpg
    image_path: assets/images/gallery/003/2008_Helsinki_048-th.jpg
gallery-004:
  - url: https://www.jabsserver.net/gallery/004/2008_Helsinki_061.jpg
    image_path: assets/images/gallery/004/2008_Helsinki_061-th.jpg
  - url: https://www.jabsserver.net/gallery/004/2008_Helsinki_062.jpg
    image_path: assets/images/gallery/004/2008_Helsinki_062-th.jpg
gallery-005:
  - url: https://www.jabsserver.net/gallery/005/200902_Helsinki_Arboretum_005.jpg
    image_path: assets/images/gallery/005/200902_Helsinki_Arboretum_005-th.jpg
  - url: https://www.jabsserver.net/gallery/005/200902_Helsinki_Arboretum_008.jpg
    image_path: assets/images/gallery/005/200902_Helsinki_Arboretum_008-th.jpg
  - url: https://www.jabsserver.net/gallery/005/200902_Helsinki_Arboretum_029.jpg
    image_path: assets/images/gallery/005/200902_Helsinki_Arboretum_029-th.jpg
  - url: https://www.jabsserver.net/gallery/005/200902_Helsinki_Arboretum_032.jpg
    image_path: assets/images/gallery/005/200902_Helsinki_Arboretum_032-th.jpg
  - url: https://www.jabsserver.net/gallery/005/200902_Helsinki_Arboretum_035.jpg
    image_path: assets/images/gallery/005/200902_Helsinki_Arboretum_035-th.jpg
gallery-006:
  - url: https://www.jabsserver.net/gallery/006/200902_Helsinki_Arboretum_011.jpg
    image_path: assets/images/gallery/006/200902_Helsinki_Arboretum_011-th.jpg
  - url: https://www.jabsserver.net/gallery/006/200902_Helsinki_Arboretum_017.jpg
    image_path: assets/images/gallery/006/200902_Helsinki_Arboretum_017-th.jpg
  - url: https://www.jabsserver.net/gallery/006/200902_Helsinki_Arboretum_022.jpg
    image_path: assets/images/gallery/006/200902_Helsinki_Arboretum_022-th.jpg
  - url: https://www.jabsserver.net/gallery/006/200902_Helsinki_Arboretum_026.jpg
    image_path: assets/images/gallery/006/200902_Helsinki_Arboretum_026-th.jpg
gallery-007:
  - url: https://www.jabsserver.net/gallery/007/200902_Helsinki_Arboretum_008.jpg
    image_path: assets/images/gallery/007/200902_Helsinki_Arboretum_008-th.jpg
  - url: https://www.jabsserver.net/gallery/007/200902_Helsinki_Arboretum_016.jpg
    image_path: assets/images/gallery/007/200902_Helsinki_Arboretum_016-th.jpg
  - url: https://www.jabsserver.net/gallery/007/200902_Helsinki_Arboretum_021.jpg
    image_path: assets/images/gallery/007/200902_Helsinki_Arboretum_021-th.jpg
  - url: https://www.jabsserver.net/gallery/007/200902_Helsinki_Arboretum_038.jpg
    image_path: assets/images/gallery/007/200902_Helsinki_Arboretum_038-th.jpg
gallery-008:
  - url: https://www.jabsserver.net/gallery/008/200905_Nuuksio_002.jpg
    image_path: assets/images/gallery/008/200905_Nuuksio_002-th.jpg
  - url: https://www.jabsserver.net/gallery/008/200905_Nuuksio_003.jpg
    image_path: assets/images/gallery/008/200905_Nuuksio_003-th.jpg
  - url: https://www.jabsserver.net/gallery/008/200905_Nuuksio_005.jpg
    image_path: assets/images/gallery/008/200905_Nuuksio_005-th.jpg
  - url: https://www.jabsserver.net/gallery/008/200905_Nuuksio_008.jpg
    image_path: assets/images/gallery/008/200905_Nuuksio_008-th.jpg
  - url: https://www.jabsserver.net/gallery/008/IMG_20200912_071448.jpg
    image_path: assets/images/gallery/008/IMG_20200912_071448-th.jpg
gallery-009:
  - url: https://www.jabsserver.net/gallery/009/200913_Suomenlinna_002.jpg
    image_path: assets/images/gallery/009/200913_Suomenlinna_002-th.jpg
  - url: https://www.jabsserver.net/gallery/009/200913_Suomenlinna_016.jpg
    image_path: assets/images/gallery/009/200913_Suomenlinna_016-th.jpg
  - url: https://www.jabsserver.net/gallery/009/200913_Suomenlinna_019.jpg
    image_path: assets/images/gallery/009/200913_Suomenlinna_019-th.jpg
  - url: https://www.jabsserver.net/gallery/009/200913_Suomenlinna_025.jpg
    image_path: assets/images/gallery/009/200913_Suomenlinna_025-th.jpg
gallery-010:
  - url: https://www.jabsserver.net/gallery/010/200913_Suomenlinna_003.jpg
    image_path: assets/images/gallery/010/200913_Suomenlinna_003-th.jpg
  - url: https://www.jabsserver.net/gallery/010/200913_Suomenlinna_013.jpg
    image_path: assets/images/gallery/010/200913_Suomenlinna_013-th.jpg
  - url: https://www.jabsserver.net/gallery/010/200913_Suomenlinna_020.jpg
    image_path: assets/images/gallery/010/200913_Suomenlinna_020-th.jpg
  - url: https://www.jabsserver.net/gallery/010/200913_Suomenlinna_029.jpg
    image_path: assets/images/gallery/010/200913_Suomenlinna_029-th.jpg
gallery-011:
  - url: https://www.jabsserver.net/gallery/011/200920_Botanical_Garden_002.jpg
    image_path: assets/images/gallery/011/200920_Botanical_Garden_002-th.jpg
  - url: https://www.jabsserver.net/gallery/011/200920_Botanical_Garden_011.jpg
    image_path: assets/images/gallery/011/200920_Botanical_Garden_011-th.jpg
  - url: https://www.jabsserver.net/gallery/011/200920_Botanical_Garden_013.jpg
    image_path: assets/images/gallery/011/200920_Botanical_Garden_013-th.jpg
  - url: https://www.jabsserver.net/gallery/011/200920_Botanical_Garden_017.jpg
    image_path: assets/images/gallery/011/200920_Botanical_Garden_017-th.jpg
gallery-012:
  - url: https://www.jabsserver.net/gallery/012/201002_Oodi_001.jpg
    image_path: assets/images/gallery/012/201002_Oodi_001-th.jpg
  - url: https://www.jabsserver.net/gallery/012/201002_Oodi_011.jpg
    image_path: assets/images/gallery/012/201002_Oodi_011-th.jpg
  - url: https://www.jabsserver.net/gallery/012/201002_Oodi_013.jpg
    image_path: assets/images/gallery/012/201002_Oodi_013-th.jpg
gallery-013:
  - url: https://www.jabsserver.net/gallery/013/201002_Oodi_032.jpg
    image_path: assets/images/gallery/013/201002_Oodi_032-th.jpg
gallery-014:
  - url: https://www.jabsserver.net/gallery/014/201002_Oodi_024.jpg
    image_path: assets/images/gallery/014/201002_Oodi_024-th.jpg
  - url: https://www.jabsserver.net/gallery/014/201004_Autumn_002.jpg
    image_path: assets/images/gallery/014/201004_Autumn_002-th.jpg
gallery-015:
  - url: https://www.jabsserver.net/gallery/015/201003_Nuuksio_001.jpg
    image_path: assets/images/gallery/015/201003_Nuuksio_001-th.jpg
  - url: https://www.jabsserver.net/gallery/015/201003_Nuuksio_015.jpg
    image_path: assets/images/gallery/015/201003_Nuuksio_015-th.jpg
gallery-016:
  - url: https://www.jabsserver.net/gallery/016/201003_Nuuksio_014.jpg
    image_path: assets/images/gallery/016/201003_Nuuksio_014-th.jpg
  - url: https://www.jabsserver.net/gallery/016/201003_Nuuksio_022.jpg
    image_path: assets/images/gallery/016/201003_Nuuksio_022-th.jpg
  - url: https://www.jabsserver.net/gallery/016/201003_Nuuksio_024.jpg
    image_path: assets/images/gallery/016/201003_Nuuksio_024-th.jpg
gallery-017:
  - url: https://www.jabsserver.net/gallery/017/201010_Nuuksio_Cabin_002.jpg
    image_path: assets/images/gallery/017/201010_Nuuksio_Cabin_002-th.jpg
  - url: https://www.jabsserver.net/gallery/017/201010_Nuuksio_Cabin_006.jpg
    image_path: assets/images/gallery/017/201010_Nuuksio_Cabin_006-th.jpg
  - url: https://www.jabsserver.net/gallery/017/201010_Nuuksio_Cabin_010.jpg
    image_path: assets/images/gallery/017/201010_Nuuksio_Cabin_010-th.jpg
gallery-018:
  - url: https://www.jabsserver.net/gallery/018/IMG_20201120_105522.jpg
    image_path: assets/images/gallery/018/IMG_20201120_105522-th.jpg
  - url: https://www.jabsserver.net/gallery/018/IMG_20201120_110526.jpg
    image_path: assets/images/gallery/018/IMG_20201120_110526-th.jpg
gallery-019:
  - url: https://www.jabsserver.net/gallery/019/IMG_20201217_161422.jpg
    image_path: assets/images/gallery/019/IMG_20201217_161422-th.jpg
  - url: https://www.jabsserver.net/gallery/019/IMG_20201217_162348.jpg
    image_path: assets/images/gallery/019/IMG_20201217_162348-th.jpg
  - url: https://www.jabsserver.net/gallery/019/IMG_20201217_232008.jpg
    image_path: assets/images/gallery/019/IMG_20201217_232008-th.jpg
gallery-020:
  - url: https://www.jabsserver.net/gallery/020/201227_Rovaniemi_003.jpg
    image_path: assets/images/gallery/020/201227_Rovaniemi_003-th.jpg
  - url: https://www.jabsserver.net/gallery/020/201227_Rovaniemi_017.jpg
    image_path: assets/images/gallery/020/201227_Rovaniemi_017-th.jpg
  - url: https://www.jabsserver.net/gallery/020/201227_Rovaniemi_023.jpg
    image_path: assets/images/gallery/020/201227_Rovaniemi_023-th.jpg
gallery-021:
  - url: https://www.jabsserver.net/gallery/021/201227_Rovaniemi_011.jpg
    image_path: assets/images/gallery/021/201227_Rovaniemi_011-th.jpg
  - url: https://www.jabsserver.net/gallery/021/201227_Rovaniemi_013.jpg
    image_path: assets/images/gallery/021/201227_Rovaniemi_013-th.jpg
gallery-022:
  - url: https://www.jabsserver.net/gallery/022/201228_Santa_Village_008.jpg
    image_path: assets/images/gallery/022/201228_Santa_Village_008-th.jpg
  - url: https://www.jabsserver.net/gallery/022/201228_Santa_Village_010.jpg
    image_path: assets/images/gallery/022/201228_Santa_Village_010-th.jpg
  - url: https://www.jabsserver.net/gallery/022/201228_Santa_Village_014.jpg
    image_path: assets/images/gallery/022/201228_Santa_Village_014-th.jpg
  - url: https://www.jabsserver.net/gallery/022/201228_Santa_Village_018.jpg
    image_path: assets/images/gallery/022/201228_Santa_Village_018-th.jpg
  - url: https://www.jabsserver.net/gallery/022/201228_Santa_Village_024.jpg
    image_path: assets/images/gallery/022/201228_Santa_Village_024-th.jpg
gallery-023:
  - url: https://www.jabsserver.net/gallery/023/201229_Snow_Shoes_015.jpg
    image_path: assets/images/gallery/023/201229_Snow_Shoes_015-th.jpg
  - url: https://www.jabsserver.net/gallery/023/201229_Snow_Shoes_036.jpg
    image_path: assets/images/gallery/023/201229_Snow_Shoes_036-th.jpg
  - url: https://www.jabsserver.net/gallery/023/201229_Snow_Shoes_050.jpg
    image_path: assets/images/gallery/023/201229_Snow_Shoes_050-th.jpg
gallery-024:
  - url: https://www.jabsserver.net/gallery/024/201229_Snow_Shoes_006.jpg
    image_path: assets/images/gallery/024/201229_Snow_Shoes_006-th.jpg
  - url: https://www.jabsserver.net/gallery/024/201229_Snow_Shoes_017.jpg
    image_path: assets/images/gallery/024/201229_Snow_Shoes_017-th.jpg
  - url: https://www.jabsserver.net/gallery/024/201229_Snow_Shoes_028.jpg
    image_path: assets/images/gallery/024/201229_Snow_Shoes_028-th.jpg
gallery-025:
  - url: https://www.jabsserver.net/gallery/025/201229_Snow_Shoes_047.jpg
    image_path: assets/images/gallery/025/201229_Snow_Shoes_047-th.jpg
  - url: https://www.jabsserver.net/gallery/025/201229_Snow_Shoes_055.jpg
    image_path: assets/images/gallery/025/201229_Snow_Shoes_055-th.jpg
gallery-026:
  - url: https://www.jabsserver.net/gallery/026/201230_Skiing_006.jpg
    image_path: assets/images/gallery/026/201230_Skiing_006-th.jpg
  - url: https://www.jabsserver.net/gallery/026/201230_Skiing_015.jpg
    image_path: assets/images/gallery/026/201230_Skiing_015-th.jpg
  - url: https://www.jabsserver.net/gallery/026/201230_Skiing_019.jpg
    image_path: assets/images/gallery/026/201230_Skiing_019-th.jpg
gallery-027:
  - url: https://www.jabsserver.net/gallery/027/201231_Northernmost_006.jpg
    image_path: assets/images/gallery/027/201231_Northernmost_006-th.jpg
  - url: https://www.jabsserver.net/gallery/027/201231_Northernmost_007.jpg
    image_path: assets/images/gallery/027/201231_Northernmost_007-th.jpg
  - url: https://www.jabsserver.net/gallery/027/201231_Northernmost_008.jpg
    image_path: assets/images/gallery/027/201231_Northernmost_008-th.jpg
gallery-028:
  - url: https://www.jabsserver.net/gallery/028/210107_Porvoo_028.jpg
    image_path: assets/images/gallery/028/210107_Porvoo_028-th.jpg
  - url: https://www.jabsserver.net/gallery/028/210107_Porvoo_042.jpg
    image_path: assets/images/gallery/028/210107_Porvoo_042-th.jpg
  - url: https://www.jabsserver.net/gallery/028/210107_Porvoo_063.jpg
    image_path: assets/images/gallery/028/210107_Porvoo_063-th.jpg
gallery-029:
  - url: https://www.jabsserver.net/gallery/029/210107_Porvoo_045.jpg
    image_path: assets/images/gallery/029/210107_Porvoo_045-th.jpg
  - url: https://www.jabsserver.net/gallery/029/210107_Porvoo_051.jpg
    image_path: assets/images/gallery/029/210107_Porvoo_051-th.jpg
  - url: https://www.jabsserver.net/gallery/029/210107_Porvoo_058.jpg
    image_path: assets/images/gallery/029/210107_Porvoo_058-th.jpg
  - url: https://www.jabsserver.net/gallery/029/210107_Porvoo_077.jpg
    image_path: assets/images/gallery/029/210107_Porvoo_077-th.jpg
gallery-030:
  - url: https://www.jabsserver.net/gallery/030/210107_Porvoo_084.jpg
    image_path: assets/images/gallery/030/210107_Porvoo_084-th.jpg
gallery-031:
  - url: https://www.jabsserver.net/gallery/031/220326_Tallinn_007.jpg
    image_path: assets/images/gallery/031/220326_Tallinn_007-th.jpg
  - url: https://www.jabsserver.net/gallery/031/220326_Tallinn_014.jpg
    image_path: assets/images/gallery/031/220326_Tallinn_014-th.jpg
  - url: https://www.jabsserver.net/gallery/031/220326_Tallinn_017.jpg
    image_path: assets/images/gallery/031/220326_Tallinn_017-th.jpg
# <insert-mark-1>
---

[//]: <> (insert-mark-2)
{% include gallery id="gallery-031" caption="The old town of Tallinn" %}
{% include gallery id="gallery-030" caption="Nordic Fence" %}
{% include gallery id="gallery-029" caption="Huts in Porvoo" %}
{% include gallery id="gallery-028" caption="The old town of Porvoo" %}
{% include gallery id="gallery-027" caption="Reindeer" %}
{% include gallery id="gallery-026" caption="Skiing in Lappland" %}
{% include gallery id="gallery-025" caption="Winter Landscapes" %}
{% include gallery id="gallery-024" caption="Snow Shoeing in Lappland" %}
{% include gallery id="gallery-023" caption="Winter Wonderland" %}
{% include gallery id="gallery-022" caption="Huskies in Rovaniemi" %}
{% include gallery id="gallery-021" caption="Arktikum Rovaniemi" %}
{% include gallery id="gallery-020" caption="Christmas in Lappland" %}
{% include gallery id="gallery-019" caption="Christmas Time" %}
{% include gallery id="gallery-018" caption="First Snow" %}
{% include gallery id="gallery-017" caption="Cabinlife" %}
{% include gallery id="gallery-016" caption="Forest landscapes" %}
{% include gallery id="gallery-015" caption="Forest vibes" %}
{% include gallery id="gallery-014" caption="Autumn colours" %}
{% include gallery id="gallery-013" caption="Bikelife" %}
{% include gallery id="gallery-012" caption="Public Library Helsinki (Oodi)" %}
{% include gallery id="gallery-011" caption="Helsinki Botanical Garden" %}
{% include gallery id="gallery-010" caption="Helsinki Coastline" %}
{% include gallery id="gallery-009" caption="Skerry landscapes on Suomenlinna" %}
{% include gallery id="gallery-008" caption="Nuuksio National Park" %}
{% include gallery id="gallery-007" caption="Panoramas" %}
{% include gallery id="gallery-006" caption="Autum is comming" %}
{% include gallery id="gallery-005" caption="Helsinki is supprisingly green" %}
{% include gallery id="gallery-004" caption="Helsinki Harbour" %}
{% include gallery id="gallery-003" caption="Impressions from Helsinki" %}
{% include gallery id="gallery-002" caption="Churches in Helsinki" %}
{% include gallery id="gallery-001" caption="Evening in Helsinki" %}
