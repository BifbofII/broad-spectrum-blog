---
title:  "Sonnenschein in Helsinki"
ref: sunshine
category: finland
verse: Ich bin das Licht der Welt. Wer mir nachfolgt, der wird nicht wandeln in der Finsternis, sondern wird das Licht des Lebens haben. --- **Johannes 8,12**
header:
  overlay_image: assets/images/headers/snow_bench.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Eine schneebedeckte Bank
---
Helsinki liegt im Norden.
Die meisten Leute wissen das vermutlich.
Tatsächlich ist es die nördlichste aller Hauptstädte von EU Mitgliedsstaaten und auf dem zweiten Platz aller Hauptstädte weltweit, wo es nur von Islands Reykjavík geschlagen wird.
Hier ist eine Karte, die ein wenig in Perspektive setzt, wie nördlich Helsinki tatsächlich liegt.
Flensburg, als nördlichste Stadt Deutschlands schafft es dabei gerade so in den Bildausschnitt.

![Karter nördlicher Städte](/assets/images/baltic_sea.png "Karte nördlicher Städte")

Und mit 60 Grad nördlicher Breite ist Helsinki noch so ziemlich der südlichste Punkt Finnlands.
Das Land erstreckt sich bis auf 70 Grad und daher liegt fast die Hälfte davon nördlich des Polarkreises.
Diese nördliche Lage führt zu einer deutlich höheren Varianz in der Länge der Tage über die Jahreszeiten hinweg als dies in Zentraleuropa der Fall ist.
Die Länge eines Tages in Helsinki variiert zwischen fast 19 Stunden im Sommer und nicht einmal 6 Stunden im Winter.
Das bedeutet, dass im Dezember die Sonne nicht vor 9 Uhr morgens auf- und schon kurz nach 3 Uhr nachmittags wieder untergeht.
All dies kann in folgendem Sonnendiagramm, in welchem gelb dem Tageslicht, Rot der Morgendämmerung, blau der Abenddämmerung und grau der Dunkelheit entsprechen, gesehen werden.

![Sonnendiagramm Helsinki](/assets/images/sun_graph_helsinki.png "Sonnendiagramm Helsinki")

Und nur zum Spaß, und weil die Diagramme so schön aussehen, hier noch eines aus Nord-Finnland.

![Sonnendiagramm Lappland](/assets/images/sun_graph_lappi.png "Sonnendiagramm Lappland")

Das fehlende Tageslicht im Winter kann negative Effekte auf die Motivation und die psychische Gesundheit haben.
Ich selbst stelle auch fest, dass ich, wenn es um 4 Uhr nachmittags bereits dunkel ist, Schwierigkeiten habe mich zu motivieren noch Dinge zu erledigen.
Jedoch ist das finnische Gesundheitssystem und die Gesellschaft, zumindest so weit ich das einschätzen kann, mehr als das deutsche auf psychische Gesundheit fokussiert.
Das geht so weit, dass es zum Beispiel an der Universität Informationsveranstaltungen zum Thema Winterdepression und einfach zu findende Verweise zu Anlaufstellen für psychische Hilfe gibt.
Zusätzlich ist es relativ üblich Vitamin D Supplements zu nehmen, da die Menge an finnischem Tageslicht nicht ausreicht, dass der Körper aus der UV-Strahlung genügend Vitamin D produzieren kann.
Ich könnte jetzt versuchen einige Dinge zu Vitamin D Supplements zusammenzufassen, da dies jedoch erst kürzlich von [maiLab, weit ausführlicher gemacht wurde, als ich das je tun könnte](https://www.youtube.com/watch?v=ud9d5cMDP_0), empfehle ich hier nur dieses Video anzuschauen.

Noch ein letzter Gedanke zum Sonnenschein in Finnland: Wie ich erst heute wieder feststellen durfte ist es, wenn die Sonne denn auch im Winter mal herauskommt (was aufgrund von Wolken noch seltener ist) ist Finnland ein wunderschönes Land.
In der Galerie gibt es einige neue Bilder vom ersten Schnee dieser Saison.