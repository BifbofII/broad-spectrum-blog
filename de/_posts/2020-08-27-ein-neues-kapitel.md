---
title:  "Ein neues Kapitel"
ref: new-chapter
category: personal
verse: Ein jegliches hat seine Zeit, und alles Vorhaben unter dem Himmel hat seine Stunde. --- **Prediger 3,1**
header:
  overlay_image: assets/images/headers/chapter.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Ein offenes Buch
---
Manchmal gibt es im Leben eine große Veränderung.
Beispielsweise bei einem Umzug, wenn man ein Studium oder einen neuen Beruf anfängt.
Ich stelle mir diese Veränderungen gerne als neue Kapitel meines Lebens vor.
Jedes einzelne ist gewissermaßen in sich selbst abgeschlossen und wenn ein neues Kapitel anfängt, wird Altes zurückgelassen und neue Dinge beginnen.
Zusätzlich gibt es unterschiedliche Grade von Kapiteln (Kapitel, Abschnitte, Unterabschnitte, ...) die verschiedene Grade von Veränderungen im Leben repräsentieren.

Kürzlich habe ich ein neues Kapitel meines Lebens begonnen.
Vermutlich ist dies das größte neue Kapitel bis jetzt in meinem Leben.
Nachdem ich meinen Bachelor im Februar dieses Jahres abgeschlossen hatte, nahm ich mir vor für mein Masterstudium etwas anderes zu machen.
So studiere ich nun an der Universität Helsinki, 1600 km entfernt von meiner Heimat in Süddeutschland.

# Alles neu
Wenn die Kapitel meines Lebens Überschriften hätten, würde das letzte Jahr vermutlich _Der Übergang_ heißen.
In dieser Zeit veränderte ich mein Studium langsam von Mechatronik zu Informatik, worauf ich mich in Zukunft fokussieren wollte.
Zusätzlich gab es viele Dinge für den großen Umzug vorzubereiten.

Und nun beginnt das Kapitel _Alles neu_.
Ich bin nach Finnland umgezogen und das Semester steht in den Startlöchern.
Dieser Kapitelwechsel ist ein besonders großer für mich, da nahezu nichts unverändert bleibt.
Bisher studierte ich in der Nähe meines Heimatortes, was bedeutete, dass ich bei meinen Eltern lebte.
Des Weiteren habe ich nie im Ausland gelebt oder an einem Ort an dem ich die Sprache nicht sprach.
Mein Bachelorstudium machte ich an einer kleinen Fachhochschule in Mechatronik und nun studiere ich, laut einigen Rankings, an einer der besten 100 Universitäten weltweit Informatik.

Alle diese Dinge definieren nun das neue Kapitel meines Lebens und ich bin gespannt, welche Erfahrungen ich in den kommenden zwei Jahren hier machen werde.

Aber mit all dem Neuen kommen auch Dinge, die ich zurücklassen muss.
Da ich bisher mein ganzes Leben nur in einem Dorf gelebt habe, stelle ich erst jetzt langsam fest wie wichtig manche der mich umgebenden Dinge für mich waren.
Es ist komisch durch die Stadt zu laufen und niemanden zu kennen.
Auch, wenn ich noch Kontakt zu meinen Freunden von Zuhause habe sind sie doch nicht hier und ich kann sie nicht treffen.
Die Kirche und der CVJM in denen ich mich ehrenamtlich engagiert habe sind nicht hier.
Und so geht die Liste weiter und wächst langsam während sich mein Gefühl langsam von Urlaubsstimmung dahin verändert, dass ich realisiere, dass ich nun hier lebe.

# Aber warum?
Nun stellt sich die Frage, die mir auch hier schon ein paar mal gestellt wurde: "Warum"?
Warum habe ich mich entschieden im Ausland zu studieren, deutsche Universitäten sind doch nicht schlecht?
Und warum Finnland?

Nun, ich war schon seit einiger Zeit interessiert an der fennoskandinavischen (ich habe letztens gelernt, dass der südliche Teil Finnlands nicht zu Skandinavien gehört) Landschaft und Natur und habe nach einer Möglichkeit gesucht die Gegend kennenzulernen.
Aus diesem Grund hatte ich mich an verschiedenen Universitäten in Norwegen, Schweden und Finnland beworben, wurde allerdings nur an der Universität Helsinki akzeptiert.
Wenn ich die Wahl gehabt hätte, wäre ich vermutlich nicht nach Finnland gegangen, jedoch nur weil ich nicht wirklich etwas über das Land wusste.
Da ich nun hier bin und mich ein wenig über Finnland informiert habe bin ich interessiert daran dieses Land im Norden besser kennenzulernen.

Abgesehen davon denke ich, die Erfahrung im Ausland zu leben ist sehr wertvoll.
Und es gibt wenige Zeitpunkte an denen dies einfacher umzusetzen ist als während dem Studium.
Theoretisch ist ein Auslandsaufenthalt auch während des Berufslebens möglich, dies kommt allerdings mit zusätzlichen Hindernissen.

# Über diesen Blog
Ich habe mich dazu entschieden diesen Blog zu beginnen, um eine Möglichkeit zu haben die Gedanken in meinem Kopf aufzuschreiben.
Vermutlich wird der Blog nicht ausschließlich auf meine Erfahrungen hier in Helsinki fokussiert sein, früher oder später werden einige andere Themen dazu kommen.
Mehr Informationen dazu gibt es auf [dieser Info-Seite](/de/about).