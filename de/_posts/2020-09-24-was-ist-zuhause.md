---
title:  "Was ist Zuhause?"
ref: what-is-home
category: personal
verse: Denn wir haben hier keine bleibende Stadt, sondern die zukünftige suchen wir. --- **Hebräer 13,14**
header:
  overlay_image: assets/images/headers/home.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Ein Hauseingang
---
Ich denke es ist ziemlich offensichtlich, warum ich in letzter Zeit angefangen habe darüber nachzudenken was _Zuhause_ ist.
Ein Umzug bringt einen in eine Position zwischen zwei Stühlen, man hängt zwischen dem alten und dem neuen Wohnort.
In den ersten ein oder zwei Wochen hier in meinem neuen Zimmer habe ich mich gefühlt als wäre ich im Urlaub.
Alles fühlte sich vorübergehend an, wahrscheinlich nicht zuletzt, weil ich noch darauf wartete, dass meine Möbel ankommen würden.
Die Wohnung hier in Helsinki und die gesamte Stadt waren noch weit davon entfernt sich wie Zuhause anzufühlen.

Nun, nachdem ich schon etwas mehr als einen Monat hier bin, beginne ich mich wohl zu fühlen.
Ich habe meine Möbel und es ist Essen in der Küche, ich finde mich in der Stadt zurecht und muss nicht jedes Mal Dinge erst kaufen, die ich im Alltag benötige.
Aber fühle ich mich zu Hause?
Ich weiß es nicht.

# Einige Definitionen von Zuhause
Das Oxford Wörterbuch definiert Zuhause folgendermaßen:

> Der Platz an dem man langfristig wohnt, im Speziellen als Mitglied einer Familie oder eines Haushaltes.

Nun denn, ich wohne langfristig hier in Helsinki, aber meine Familie ist in Deutschland.
Des Weiteren weist mich ein Aufkleber auf meinem Personalausweis darauf hin, dass ich keinen Wohnsitz in Deutschland habe.
Aber natürlich erfasst diese Definition nicht wirklich was wir meist meinen, wenn wir von _Zuhause_ sprechen.

Eine weitere übliche Definition bezeichnet Zuhause als eine _bekannt und gewohnte Umgebung_.
Das liegt näher an dem woran ich denke, wenn ich sage, dass ich nicht weiß ob Helsinki bereits mein Zuhause ist.
Es beginnt definitiv sich bekannt anzufühlen, aber noch nicht im selben Maß wie es Deutschland würde.

Die zweite Definition ist ähnlich zu etwas das ich aus dem TED Vortrag [_Where is home? (Wo ist Zuhause?)_ von Pico Iyer](https://www.ted.com/talks/pico_iyer_where_is_home) entnommen habe.

> Wir wissen, dass das Zuhause nicht einfach mehr der Ort ist, wo man geboren wurde.
> Es ist ein Ort, an dem man sich selbst verwirklicht.

Zu Beginn seines Vortrags gibt er einen kurzen Überblick über seinen kulturellen Hintergrund, welcher deutlich mehr Länder beinhaltet als meiner.
Trotzdem beschreibt er seinen Hintergrund als 'altmodisch und überschaubar'.
Was sind nun also meine Gedanken zu der Frage was Zuhause ist, mit einem noch überschaubareren Hintergrund?

# Was Zuhause für mich ist
Bis vor kurzem war es für mich sehr einfach zu sagen was mein Zuhause war.
Ich bin nicht ein einziges Mal umgezogen bevor ich nach Helsinki kam, daher war Zuhause immer im Südwesten Deutschlands.
Meine gesamte Familie ist dort, die meisten meiner Freunde ebenfalls und ich habe nicht viel anderes gekannt.

Nun da ich hier bin, würde ich Deutschland immer noch als mein Zuhause bezeichnen.
Ergänzend habe ich durch den Umzug einen anderen Blick auf Dinge gewonnen, die ich als selbstverständlich angesehen hatte und von denen ich nun feststelle, wie wichtig sie für mich waren.
In diesem Sinne kann das Verlassen des Zuhauses die Verbindung zu diesem auch stärken.

Bezüglich der Frage ob Helsinki in den zwei Jahren, die ich hier sein werde, zu einem Zuhause für mich werden wird: vielleicht komme ich in ein paar Monaten auf diese Frage zurück, im Moment kann ich nicht besonders viel dazu sagen.
Eine Sache, die bereits ein bisschen wie ein Zuhause für mich ist, ist die Kirche, die ich gefunden habe.
Vom ersten Moment an habe ich mich dort willkommen gefühlt und es gab Leute, die sich mit mir unterhalten haben.
Es ist angenehm Leute um einen zu haben die denselben Glauben teilen und zu denen man daher eine Verbindung hat.

Ich möchte diesen Blog Post mit einem weiteren Gedanken aus dem oben erwähnten TED Vortrag abschließen:

> Wo man herkommt, ist heutzutage weniger wichtig, als wo man hingeht.

Im Kontext des Vortrags ist die Idee, dass der persönliche Ursprung weniger Einfluss auf einen selbst und auf sein Zuhause hat als die Dinge, nach denen man strebt.
Das erinnert mich an Hebräer 13,14 wo der Autor des Briefes ausdrückt, dass alles, was wir hier auf der Erde haben uns nicht für immer bleiben wird.
Was uns als Christen definiert ist das, was im Anschluss kommt.
Das ist unser wahres Zuhause.