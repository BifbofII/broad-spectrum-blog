---
title:  "Einige Eindrücke aus Finnland"
ref: impressions
category: finland
verse: Er war vor allem anderen da, und alles besteht durch ihn. --- **Kolosser 1,17**
header:
  overlay_image: assets/images/headers/lake.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Ein See
---
Nachdem ich nun fast zwei Monate hier in Finnland bin, möchte ich ein paar Eindrücke über das Land, die Kultur und die Sprache zusammenfassen.
Ich hoffe, dass einige dieser Dinge interessant für diejenigen sind welche, wie ich bevor ich hier herkam, nicht viel über Finnland wissen.

# Sprache
Einer der vermutlich am besten bekannten Fakten über Finnland ist, dass die finnische Sprache recht speziell und nicht mit den meisten anderen Sprachen vergleichbar ist.
Laut [Wikipedia](https://de.wikipedia.org/wiki/Finnische_Sprache) gehört Finnisch zu den Uralischen Sprachen.
Abgesehen von Finnisch gehören Ungarisch und einige Minderheitensprachen (inklusive Sámi, was vom Volk der Samen in Nord-Finnland gesprochen wird) zu dieser Sprachfamilie.
Dies erklärt, warum die Sprache so unterschiedlich gegenüber der meisten, uns bekannteren Sprachen ist, da diese fast alle zu den indoeuropäischen Sprachen gehören.

Der erste deutliche Unterschied ist, dass die meisten Worte nicht ähnlich deren deutschen, englischen oder französischen Übersetzungen gegenüber sind und der Wortschatz sich daher schwieriger aneignen lässt.
Ein Beispiel hierfür ist das Wort Sonne (_en_: sun, _fr_: soleil), welches auf Finnisch aurinko heißt.
Bei weiterer Betrachtung wird jedoch klar, dass sich nicht nur der Wortschatz, sondern auch die Grammatik deutlich unterscheidet.
Dinge wie, dass das verneinende Wort 'nicht' mehr wie ein Verb ist und im Finnischen konjugiert werden muss oder, dass das Wort 'mit' eine grammatikalische Konstruktion basierend auf dem Genitiv und einem angehängten Wort ist sind anfangs schwer zu begreifen.

Was jedoch recht hilfreich im Alltag ist, ist, dass Finnland offiziell ein bilinguales Land ist.
Schwedisch ist die zweite offizielle Sprache, was bedeutet, dass die meisten Schilder auf Finnisch und Schwedisch geschrieben sind.
Daher können viele geschriebene Informationen (als deutscher Muttersprachler) auf Basis des Schwedischen, welches dem Deutschen und Englischen deutlich näher ist, erraten werden.
Zum Glück sprechen die meisten Finnen jedoch sehr gutes Englisch, was es sehr einfach macht, jemanden um Hilfe zu bitten.

# Bildungssystem
Weltweit wird das finnische Bildungssystem als eines der besten angesehen.
Finnland schneidet regelmäßig als eines der besten Länder in den [PISA-Studien](https://de.wikipedia.org/wiki/PISA-Studien) ab und der [Index der menschlichen Entwicklung](https://de.wikipedia.org/wiki/Index_der_menschlichen_Entwicklung) Finnlands ist ebenfalls einer der besten, was dem nationalen Bildungssystem zugeschrieben wird.

Ich kann persönlich nur ein paar Eindrücke darüber geben was ich in meiner ersten Periode an der Universität Helsinki wahrgenommen habe.
Im Gegensatz zu deutschen Universitäten würde ich sagen, dass Studenten an finnischen Universitäten mehr direkten Kontakt zu Professoren und Professorinnen haben.
Mir sind keine offiziellen Zahlen bekannt, wie sich das Verhältnis von Studenten zu Professuren zwischen Deutschland und Finnland verhält, ich würde jedoch vermuten, dass das Verhältnis an finnischen Universitäten dem an deutschen Hochschulen ähnlicher ist als an deutschen Universitäten.

Zusätzlich fühlt sich die Kommunikation zwischen Studenten und Angestellten der Universität hier entspannter an.
Dies ist mir auch außerhalb der Universität, in Situationen in denen ich in Deutschland einen recht formalen Umgang erwarten würde (beispielsweise auf einem Amt), aufgefallen, an der Universität ist es jedoch sehr ersichtlich.
Jeder, von Student bis Professor und Professorin wird beim Vornamen angesprochen, es ist keine Seltenheit zu sehen, dass Angestellte in Gruppenchats in Witze von Studenten einsteigen und ich habe eine beeindruckende Anzahl von [Memes in offiziellen Vorlesungsunterlagen](/assets/images/gdpr_meme.png) gesehen.

# Die Stadt Helsinki
Da ich bisher noch in keiner anderen Stadt gelebt haben, kann ich meine Eindrücke hier nicht direkt mit anderen Städten vergleichen.
Hier sind trotzdem zwei Punkte, die mir aufgefallen sind:

Es gibt viel ins Stadtbild integrierte Natur.
Alle paar Straßenblöcke gibt es einen weiteren Park mit zumindest einigen Bäumen und Bänken um sich zu setzen und eine Pause zu machen.
Der vermutlich eindrucksvollste Park ist der [Central Park in Helsinki](https://www.myhelsinki.fi/en/see-and-do/nature/central-park-the-central-forest-running-the-length-of-helsinki), welcher sich durch komplett Helsinki erstreckt, fast von der südlichen Küste bis zum nördlichen Stadtrand, was es ermöglicht Helsinki ausschließlich im Grünen zu durchqueren.

Helsinki ist eine angenehme Stadt um sich mit dem Fahrrad fortzubewegen.
Es gibt viele Schotterwege durch die Parks oder geteerte Fahrradwege entlang der Straßen.
Die meisten Plätze in der Stadt sind sehr einfach mit dem Fahrrad zu erreichen und es gibt Stadträder welche günstig von Radstationen geliehen werden können.
Nur im Stadtzentrum kann es mit dem Rad etwas ungemütlich werden, da dort viele Straßen und Wege aus Kopfsteinpflaster sind.

Es gibt auch viele Baustellen in der ganzen Stadt verteilt.
Egal ob der Bau einer neuen Straßenbahnlinie, neuer Fahrradwege oder eine einfache Erneuerung des Straßenbelags, es scheint nicht als würde die Stadt an den Ausgaben für öffentliche Straßen sparen.

# Natur
Finnland ist zu fast [dreiviertel von Wäldern bedeckt](http://www.borealforest.org/world/world_finland.htm).
Daher ist es nicht nur möglich die nächsten Wälder, sondern sogar Nationalparks einfach mithilfe von öffentlichen Verkehrsmitteln aus Helsinki zu erreichen.
Eine Wanderung in [Nuuksio](https://www.nationalparks.fi/nuuksionp) oder einem der anderen Nationalparks stellt eine geniale Abwechslung zum Alltag in der Stadt dar.

Die zweite definierende Eigenschaft von Finnlands Natur sind Seen.
In Finnland gibt es [mehr als 180 Tausend Seen](https://de.wikipedia.org/wiki/Liste_der_gr%C3%B6%C3%9Ften_Seen_in_Finnland), wobei die kleinsten noch nicht einmal mitgezählt sind.
Bei einer Wanderung im Wald sind Picknickplätze neben einem See daher einfach zu finden.

Eine letzte Sache, die mir noch aufgefallen ist, sind die vielen Gletscherformationen.
Rund geformte Granitfelsen, Schären und Findlinge sind überall zu finden.

# Zusammenfassung
Dies sind nur einige Dinge, welche mir in meiner kurzen Zeit, die ich nun hier lebe, aufgefallen sind.
Natürlich sind diese keine allumfassende Zusammenfassung von Finnland, sondern basieren nur auf meiner Erfahrung.
Ich hoffe, dieser Post kann trotzdem einige Eindrücke geben, wie es ist in Finnland zu wohnen.
Mehr visuelle Eindrücke, vor allem aus Helsinki, können in der [Galerie](/de/gallery) gefunden werden.