---
title:  "Wie hat sich Zuhause verändert?"
ref: how-has-home-changed
category: personal
verse: Wenn der HERR nicht das Haus baut, so arbeiten umsonst, die daran bauen. Wenn der HERR nicht die Stadt behütet, so wacht der Wächter umsonst. --- **Psalm 127,1**
header:
  overlay_image: assets/images/headers/home2.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Ein Haus
---
Für die letzten drei Wochen war ich zurück in Deutschland.
Dort ist die Frage nach Zuhause wieder aufgekommen, um genau zu sein, danach wie sich Zuhause verändert hat.
In meiner Situation besteht diese Frage aus zwei verschiedenen Ebenen.
Zum einen, was hat sich Zuhause, im Sinne von in Deutschland, in dem Jahr welches ich nicht dort war verändert.
Zum anderen, wie hat sich die Definition von Zuhause für mich verändert.
Ich werde versuchen einige Gedanken zu diesen Fragen darzulegen und bezüglich der zweiten Frage einige Gedanken, welche ich bereits in meinem [Post letzten September]({% post_url de/2020-09-24-was-ist-zuhause %}) angerissen habe, auszuweiten.

Ehrlich gesagt, nicht so viel hat sich in einem Jahr verändert.
Das trifft vermutlich auf die meisten Jahre zu, aber im Speziellen auf ein Jahr, in welchem eine globale Pandemie die meisten kreativen Ideen etwas Neues zu starten verhindert hat.
Daher fühlte sich fast alles in Deutschland gewohnt an.
Ich habe viele Leute getroffen, die ich gut kenne, aber mit denen ich eine lange Zeit nicht gesprochen hatte und eine Menge Orte besucht, die ich ebenfalls noch gut kenne.
Es war ebenso sehr schön einige Projekte, an denen ich in der Kirche und dem CVJM gearbeitet habe, weitergeführt zu sehen.
Was hat sich nun jedoch am meisten verändert?
Vermutlich ich.

Trotzdem, dass sich alles in Deutschland gewohnt angefühlt hat lebe ich nicht mehr dort und das bereits seit fast einem Jahr.
In meinem Post im September über _was Zuhause ist_ wusste ich noch nicht ob Helsinki zu meinem Zuhause werden würde.
An diesem Punkt war ich erst etwa einen Monat in Helsinki und hatte noch keine Ahnung von vielen der tollen Menschen die ich hier kennenlernen und wie wohl ich mich hier fühlen würde.
Seit damals hat sich viel verändert, selbst wenn ich es in diesem Prozess nicht unbedingt wahrgenommen habe.
Die Reise zurück nach Deutschland hat meinem Hirn einen Anstoß gegeben über dieses Thema nochmals etwas nachzudenken und ich sehe inzwischen klarer, wie viel sich verändert hat.

Während der gesamten Zeit in Deutschland habe ich mich wie ein Besucher gefühlt.
Das ergibt Sinn, da ich genau das war, im Voraus war ich mir jedoch nicht so sicher, ob das so sein würde.
Es hätte sich auch sehr gut so anfühlen können, als ob ich permanent zurückkomme und dann hätte ich mich nach den drei Wochen wieder heraus reisen müssen.
Aber ich bin sehr froh, dass dem nicht so war.
Dass ich mich wie ein Besucher gefühlt habe, hat mir gezeigt, dass ich Helsinki inzwischen als mein Zuhause akzeptiert habe.
Ich bin froh, dass diese Veränderung seit September eingetreten ist, andernfalls wäre das kommende Jahr vermutlich sehr herausfordernd geworden.

Wie kann ich dies nun zusammenfassen?
Ich weiß immer noch nicht was ich als eine Definition von Zuhause verwenden würde, allerdings kann ich inzwischen sagen, dass Helsinki _meine_ momentane Heimat _ist_.
Zurück in Deutschland zu sein und Leute persönlich wiederzutreffen war natürlich sehr schön, als ich gestern jedoch den Bus vom Bahnhof zu meiner Wohnung genommen habe, hatte ich das befreiende Gefühl nach Hause zu kommen.
Ich bin froh, dass ich hier so viele Menschen mit so diversen Hintergründen kennenlernen durfte.
In diesem Jahr konnte ich trotz Covid um die 80 Personen aus mehr als 20 verschiedenen Nationalitäten kennenlernen.
Das ist ein wundervolles Geschenk, für das ich sehr dankbar bin.

Ich freue mich in dieses nächste Jahr hier in Helsinki hineinzugehen und herauszufinden was Gott für mich hier vorbereitet hat.