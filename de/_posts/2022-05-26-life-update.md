---
title:  "Ein generelles Update aus meinem Leben"
ref: life-update
category: personal
verse: "Habe ich dir nicht geboten: Sei getrost und unverzagt? Lass dir nicht grauen und entsetze dich nicht; denn der HERR, dein Gott, ist mit dir in allem, was du tun wirst. --- **Josua 1,9**"
header:
  overlay_image: assets/images/headers/lake_boat.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Ein Boot auf einem See
---
Es ist eine lange Weile her seit ich etwas hier gepostet habe, daher nehme ich mir nun endlich die Zeit euch ein generelles Update aus meinem Leben zu geben.

Das letzte Mal, dass ich etwas hier geschrieben habe, war vor fast einem ganzen Jahr als ich aus Deutschland zurückkam.
Vieles hat sich seitdem verändert, sowohl persönlich für mich als auch bezüglich meiner Arbeit und meines Studiums.
Für mich persönlich ist definitiv die größte Veränderung, dass ich nun in einer Beziehung bin, das soll jedoch nicht das sein worum es in diesem Post geht.
Hier werde ich mich darauf beschränken, euch allen ein Update bezüglich des Fortschrittes meines Studiums und meiner Pläne für die Zukunft zu geben.

Vor ziemlich genau einem Jahr, im Mai 2021, habe ich ein Sommerpraktikum in einer der Forschungsgruppen an der Universität Helsinki begonnen.
Studierende im Master verbringen hier gerne ihren Sommer damit, in Forschungspraktika mehr zu lernen, in Forschungsarbeit hineinzuschnuppern und etwas Geld zu verdienen.
I hatte mich für dieses Praktikum mit der Intention entschieden, die Frage zu beantworten, ob ich in Zukunft gerne Forschung betreiben möchte oder nicht.
Daher begann ich dieses Praktikum, welches ursprünglich bis Ende August dauern sollte.
Im Laufe des Praktikums konnte ich eng mit meinen drei Betreuern, zwei Postdoktoranden und unserem Professor, zusammen arbeiten.
Ursprünglich arbeiteten wir daran, eine effizientere Methode zum Lernen von interpretierbaren Repräsentationen von Daten zu finden.
Gegen Ende der drei Monate Praktikum hatte sich unser Fokus leicht verändert und wir hatten einen universellen Algorithmus zur Optimierung zweier in Konflikt stehender Funktionen entwickelt.

I habe im Laufe dieses Praktikums viel gelernt, im Speziellen in den letzten Wochen.
Meine Betreuer entschieden, dass es einen Versuch wert ist, unsere Arbeit als einen Forschungsartikel zusammenzufassen und bei einer internationalen Konferenz einzureichen.
Dies war eine völlig neue Erfahrung für mich.
Eventuell werde ich noch einen weiteren Post über den wissenschaftlichen Veröffentlichungsprozess schreiben, für jetzt jedoch nur eine kleine Zusammenfassung:
Wenn man möchte, dass andere von seiner Forschung erfahren, muss man sie als Artikel bei einer wissenschaftlichen Konferenz oder in einem Journal veröffentlichen.
Dafür reicht man sein Manuskript ein, um von Personen aus dem selben Forschungsfeld überprüft zu werden.
Sollten diese den Artikel für gut und interessant genug halten, wird er akzeptiert und veröffentlicht.
Machen wir es kurz, unser Artikel wurde als _nicht_ gut genug empfunden und abgelehnt, wir bekamen jedoch wertvolles Feedback, um ihn zu verbessern.
Nicht nur von den Rezensionen, sondern noch viel mehr durch das gemeinsame Schreiben mit meinen Betreuern habe ich sehr viel in diesem Prozess gelernt.
Ich konnte sehen, wie strategisch man schreiben muss, um die Chance zu erhöhen, dass ein Artikel akzeptiert wird, wie bereits veröffentlichte Artikel sehr häufig noch viel Verbesserungspotential haben und wie vielseitig Forschungsarbeit ist.
Zusammenfassen, ich habe Forschung lieben gelernt und mich daher entschieden weiterhin als Forschungsassistent in der Forschungsgruppe zu bleiben.

Vom Ende des Sommers bis zum Ende des Jahres habe ich in Teilzeit weiter gearbeitet während ich auch Vorlesungen besucht habe und seit Anfang dieses Jahres arbeite ich als Vollzeit Forschungsassistent.
Ich habe daran gearbeitet, was wir während des Praktikums erreicht hatten weiter zu verbessern und das ganze als einen neuen Artikel für eine andere Konferenz zusammenzufassen.
Und seit Ostern wissen wir, dass sich die Arbeit gelohnt hat, unser Artikel wurde akzeptiert und wird im August bei der [SAT Konferenz 2022](http://satisfiability.org/SAT22/) veröffentlicht.
Des Weiteren habe ich in den letzten zwei Monaten eine erweiterte Version des selben Artikels als meine Masterarbeit geschrieben, welche ich Anfang nächster Woche abgeben werde.
Es gibt also Fortschritte, ich werde im nächsten Monat meinen Masterabschluss erhalten und mein erster Artikel als Erstautor wird diesen Sommer auch veröffentlicht.
Ich bin sehr froh darüber, welche großartigen Möglichkeiten sich aus dem Forschungspraktikum letzten Sommer ergeben haben und empfehle etwas in dieser Art jedem in einer ähnlichen Situation.

Vermutlich die größte Entwicklung aufgrund des Sommerpraktikums sind meine Pläne für die Zukunft.
Ich habe mich entschieden, dass ich meinen Weg in der Forschung fortsetzen möchte, damit ist der nächste Schritt eine Doktorarbeit.
Dafür werde ich weiterhin mit meinen Betreuern zusammen und in meinem aktuellen Forschungsfeld arbeiten.
Das heißt auch, dass ich für in etwa weitere vier Jahre hier in Helsinki bleiben werde.
Als ich hier hergezogen bin, hatte ich den Gedanken "selbst wenn ich es hier nicht besonders mag sind zwei Jahre nicht _zu_ lange" im Kopf.
Na ja, ich mag es hier und ich bin bereit weitere vier Jahre zu bleiben.
Es gibt noch einige Details, die für die Doktorarbeit geklärt werden müssen, aber grundsätzlich läuft mein aktueller Vertrag als Forschungsassistent bis Ende August und im Anschluss daran werde ich offiziell mit der Doktorarbeit starten.
Ich werde noch einige Bewerbungen schreiben müssen, um hoffentlich direkt finanzielle Mittel zu bekommen, aber selbst wenn nicht, kann ich mit Geldmitteln aus dem Projekt meines Professors beginnen.
Eventuell sollte ich versuchen eine zugängliche Version meines Forschungsplanes (sobald dieser existiert) als einen Blogpost zu schreiben, aber ich werde nichts versprechen.

Als letzten Punkt, lasst mich etwas zu meinen (Reise-) Plänen im Sommer schreiben.
Ich werde für kurze Zeit Ende Juli in Deutschland sein, aber erst danach passieren die größeren zwei Dinge.
Direkt aus Deutschland werde ich nach Israel, zu der Konferenz, bei der unser Artikel veröffentlicht wird, fliegen und dort auch eine kurze Präsentation über den Inhalt des Artikels halten.
Ich freue mich sehr über diese Möglichkeit eine erste wissenschaftliche Konferenz zu besuchen, im Speziellen, weil diese Konferenz teil eine [großen übergeordneten Konferenz](https://www.floc2020.org/) ist, was bedeutet, dass es dort geniale Möglichkeiten geben wird, um Vorträge von einflussreichen Personen zu hören und Personen zu treffen deren Arbeit ich gelesen habe.
Daraus ergibt sich vermutlich eine weitere Möglichkeit, um meine Erfahrungen in einem Blogpost zu verarbeiten, hoffen wir also, dass ich mir in den nächsten Monaten mehr Zeit zum Schreiben nehme.
Nach Israel, und einem kurzen Zwischenstopp in Helsinki, werde ich den restlichen August in Indonesien verbringen.
Diese Reise wird ebenfalls voll von Eindrücken und Erfahrungen sein, falls ich davon nichts hier poste, fragt gerne direkt nach oder gebt mir einen Stoß, damit ich etwas schreibe und nicht eine weitere Lücke von einem Jahr entsteht.