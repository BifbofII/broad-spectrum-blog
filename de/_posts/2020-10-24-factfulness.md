---
title:  "Factfulness"
ref: factfulness
category: data-science
verse: Verlass dich auf den HERRN von ganzem Herzen, und verlass dich nicht auf deinen Verstand. --- **Sprüche 3,5**
header:
  overlay_image: assets/images/headers/data.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Graphen von Daten
---
Hier ist mein erster Post der nichts mit mir oder Finnland zu tun hat.
Kürzlich habe ich das Buch _Factfulness_ von Hans Rosling gelesen, dies sind eine kurze Zusammenfassung und einige persönliche Gedanken darüber.

# Zusammenfassung
[Hans Rosling](https://de.wikipedia.org/wiki/Hans_Rosling) war Professor für Internationale Gesundheit in Schweden, bekannt ist er jedoch nicht in erster Linie für seine medizinische Arbeit.
Seinen Bekanntheitsgrad erlangte er vielmehr durch seinen Blick und seine Arbeitsweise in Bezug auf Daten.

Er hielt viele Vorträge (wie diesen [TED talk](https://www.youtube.com/watch?v=hVimVzgtD6w)) in welchen er typischerweise animierte Blasendiagramme verwendete, um Entwicklungen über Zeiträume darzustellen und obwohl viele dieser Vorträge zu weltweiten Gesundheitsthemen waren, bekamen sie ihre Reichweite nicht durch neue medizinische Erkenntnisse.
Die gezeigten Daten aus den Vorträgen waren üblicherweise schon mehrere Jahre im voraus öffentlich verfügbar, der Unterschied bestand darin wie sie präsentiert wurden.

In seinem Buch fokussiert sich Hans Rosling auf unser Weltbild und warum wir damit so oft falsch liegen, obwohl es Daten gibt, mit denen wir feststellen könnten, dass wir falsch liegen.
Er führt eine neue Blickweise auf die Welt ein, welche er _Factfulness_ (Faktenfreude) nennt.
Diese, so sagt er, steht grundsätzlich dem entgegen wie wir Menschen instinktiv handeln.
Aber dennoch ist sie notwendig um ein korrektes Weltbild zu haben und fundierte Entscheidungen treffen zu können.

Im Laufe des Buches stellt er viele globale Themen vor, über welche die meisten Personen nicht genügend wissen, um einfache Faktenfragen zu beantworten.
Ein Beispiel für eine solche Frage ist:

> In den letzten 20 Jahren hat sich der Anteil der Weltbevölkerung, welcher in extremer Armut lebt...
> * A: fast verdoppelt.
> * B: nur sehr wenig verändert.
> * C: fast halbiert.

Das Buch enthält viele solcher Fragen und immer wieder werden Ergebnisse präsentiert welche zeigen, dass die Mehrheit die Antworten auf die Fragen nicht nur nicht kennt, sondern die meisten Leute denken sie kennen die Antwort, liegen jedoch falsch.
Um es mit dem Buch zu sagen: selbst ein Schimpanse, welcher die Antworten zufällig auswählt, würde mit einem besseren Ergebnis abschließen als die meisten Menschen.
(Übrigens, auf die obere Frage ist C die korrekte Antwort.)

In einem Versuch zu erklären, warum wir die Welt oft so grundlegend falsch wahrnehmen, definiert Hans Rosling 10 dramatische Instinkte welchen wir bei der Beantwortung von Fragen folgen, was zu falschen Annahmen führt.
Indem wir uns dieser Instinkte bewusst sind, können wir gegen diese ankämpfen und ein faktenbasierteres Weltbild erreichen.

Die 10 Instinkte sind:
* Der Instinkt der _Kluft_: Wir nehmen oft an, dass Dinge in zwei deutlich getrennte Gruppen geteilt sind, welche durch eine Kluft getrennt werden.
Dies ist jedoch meist nicht der Fall.
Meistens befinden sich die meisten Menschen, Länder, oder was auch immer wir betrachten, in der Mitte, genau dort wo wir die Kluft erwarten.
* Der Instinkt der _Negativität_: Wir bekommen oft ein negatives Weltbild da uns nur negative Nachrichten erreichen.
Positive Dinge sind meist nicht interessant genug um berichtet zu werden.
Zusätzlich können Dinge schlecht sein und sich trotzdem gleichzeitig verbessern.
* Der Instinkt der _geraden Linie_: Wenn wir eine Tendenz in Daten beobachten gehen wir üblicherweise davon aus, dass diese sich in einer geraden Linie fortsetzen wird.
* Der Instinkt der _Angst_: Wenn uns etwas Angst macht haben wir die Tendenz das Risiko dieser Sache zu hoch zu bewerten.
Risiko ist ein Faktor welcher sich aus Gefahr und Ausgesetztsein zusammensetzt, die zweite Komponente wird jedoch häufig vergessen.
* Der Instinkt der _Dimension_: Wenn wir eine einzelne Zahl betrachten, ist es üblicherweise schwer ihre tatsächliche Größe einzuschätzen ohne sie mit anderen Dingen zu vergleichen um sie in Proportion zu setzen.
* Der Instinkt der _Verallgemeinerung_: Wir lieben es Dinge in Kategorien zu gruppieren, um es einfach zu machen sie zu verstehen.
Dies ist hilfreich, wir müssen jedoch darauf achten die Kategorien sinnvoll zu wählen.
* Der Instinkt des _Schicksals_: Veränderung ist oft so klein, dass wir sie nicht wahrnehmen.
Daher gehen wir häufig davon aus, dass Dinge konstant sind und sich nie verändern werden.
* Der Instinkt der _einzigen Perspektive_: Eine einzelne Perspektive kann unsere Vorstellungskraft oft einschränken und uns zu falschen Schlüssen führen.
* Der Instinkt der _Schuldzuweisung_: Üblicherweise suchen wir nach Übeltätern anstatt nach Ursachen und nach Helden anstatt nach Systemen die Umstände verbessern.
* Der Instinkt der _Dringlichkeit_: Meistens gehen wir davon aus, dass Probleme dringender sind als es der Fall ist.
Daher kommt es oft vor, dass wir gedrängte und nicht faktenbasierte Entscheidungen treffen.

Wenn diese Dinge interessant klingen, empfehle ich es sehr für mehr Details das Buch zu lesen.

# Persönliche Gedanken
Auch wenn ich sagen würde ich mag es Fakten nachzuschlagen und Daten zu referenzieren, wenn es notwendig ist, waren meine Ergebnisse bezüglich der Faktenfragen im Buch relativ schlecht.
Ich kämpfe also definitiv selbst mit den Instinkten, die mein Weltbild verzerren.

Die Punkte, welche im Buch hervorgehoben werden, klingen jedoch relativ einfach.
Alles leuchtet ein, wenn es einem vorgelegt wird.
Die tatsächliche Umsetzung ist jedoch eine andere Sache.
Er nennt diese Dinge nicht umsonst _Instinkte_.
Ein Instinkt ist etwas das wir anwenden ohne es wirklich wahrzunehmen.
Exakt das ist was passiert, wenn wir nicht aufpassen.
Ohne aktive Anstrengungen auf die Fakten zu schauen, bleiben wir in unserem verzerrten Weltbild stecken.

Nachdem ich das Buch gelesen habe bin ich nun interessiert diese Punkte zu versuchen anzuwenden.
Ich bin gespannt herauszufinden was sich tatsächlich verändern wird.
Hoffentlich bleibt mir Factfulness im Bewusstsein und ich falle den dramatischen Instinkten nicht so oft zum Opfer.