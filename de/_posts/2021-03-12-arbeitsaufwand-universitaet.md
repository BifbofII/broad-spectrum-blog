---
title: "Zeitaufwand an der Universität"
ref: timeuseage-university
category: data-science
verse: Ein jegliches hat seine Zeit, und alles Vorhaben unter dem Himmel hat seine Stunde. --- **Prediger 3,1**
header:
  overlay_image: assets/images/headers/time.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Eine Uhr
---
Ich mag Daten und Zahlen.
Das klingt für einige vermutlich etwas schräg, aber genaue numerische Messwerte, für egal was, sind sehr zufriedenstellend für mich.
Dies ist einer der Gründe warum ich angefangen habe die Zeit, welche ich für einige Aufgaben in meinem Leben aufwende mit einem Service namens [Toggl Track](https://toggl.com/track/) aufzuzeichnen.
Mit diesem Service kann man benannte Timer mitlaufen lassen, sobald man an einer bestimmten Aufgabe arbeitet und man kann die zusammengefassten Daten im Nachhinein anschauen oder herunterladen um sie genauer zu analysieren.
Ende 2018 habe ich damit angefangen meine Zeit zu tracken und einer der Bereiche, in denen ich das am zuverlässigsten getan habe, ist die Zeit, welche ich für Vorlesungen an der Uni aufgewendet habe.
Bis zum heutigen Zeitpunkt habe ich etwa 4,5 Semester mit aufgezeichneten Zeitdaten für Vorlesungen an drei unterschiedlichen Universitäten und ich habe mich nun endlich dazu entschieden einen detaillierten Blick auf diese Daten zu werfen.

# Über das ECTS
Das [ECTS](https://de.wikipedia.org/wiki/European_Credit_Transfer_System) oder Europäisches System zur Übertragung und Akkumulierung von Studienleistungen versucht zu standardisieren wie Vorlesungen an Universitäten in ganz Europa gewichtet werden.
Hierzu wurde die Einheit eines ECTS Credits eingeführt und jede Vorlesung ist eine bestimmte Anzahl dieser Credits wert.
Alle Länder, welche das ECTS verwenden, haben gemeinsam, dass pro Studienjahr ein Ziel von 60 Credits für jeden Studierenden vorgesehen ist.
Wo sich nicht alle Länder einig sind ist, wie viele Arbeitsstunden ein Credit repräsentiert.
Diese Zahl variiert zwischen 25 und 30 Stunden pro Credit, abhängig vom Land oder sogar der Institution innerhalb eines Landes.

Die Idee eines Systems, welches Vergleichbarkeit zwischen akademischen Leistungen aus verschiedenen Ländern schafft, ist definitiv eine gute, wenn dem nicht so wäre hätte ich deutlich mehr Schwierigkeiten gehabt mein Studium hier in Finnland anzufangen.
Die Stundenzuweisung für eine Credit fühlt sich jedoch etwas aus der Luft gegriffen an.
Meist, wenn ich mit anderen Studierenden über dieses Thema gesprochen habe, bekam ich eine ähnliche Meinung zu hören, die ich von mir selbst schon kannte, nämlich, dass in Realität deutlich weniger Zeit pro Vorlesung aufgewendet wird als es die Credits angeben.
Um dieses Bauchgefühl zu überprüfen habe ich beschlossen in meine aufgezeichneten Daten zu schauen um zu sehen, ob sich dort etwas Interessantes herausfinden ließe.

# Die Daten und der Prozess
Qie oben bereits erwähnt habe ich Zeitdaten mit Toggl aufgezeichnet.
Von deren Website kann eine CSV Datei heruntergeladen werden, welche für jeden Zeiteintrag den Timer Namen (in meinem Fall der Name der Vorlesung), die Start- und die Endzeit enthält.
Nach dem Herunterladen der Daten habe ich diese in einem Jupyter Notebook, welches für Interessierte [hier](/special-pages/university-timelogs-nb) gefunden werden kann, verarbeitet, gesäubert und die Dauern für jede Vorlesung summiert.
Zusätzlich zu diesen Daten habe ich manuell für jede Vorlesung eingegeben wie viele Credits dies wert war und an welcher Universität ich diese besucht habe.
Um die Credits in geforderte Stunden umzurechnen habe ich mich entschieden einen Wert von 25 Stunden pro Credit zu verwenden, da dies der niedrigste Wert für Deutschland (25-30) und Finnland (27) ist, wo ich studiert habe.
Durch das Zusammenfügen dieser Informationen konnte ich was ich den Zeitanteil $\lambda=\frac{T_\text{aufgezeichnet}}{T_\text{ECTS}}$ berechnen.
Dieser Zeitanteil $\lambda$ gibt an wie viel der geforderten Zeit ich tatsächlich aufgewendet habe.
Die Interpretation hiervon ist folgende:

| $\lambda$ | Interpretation                                       |
| --------- | ---------------------------------------------------- |
| $1$       | Ich habe exakt die geforderte Zeit aufgewendet       |
| $1,5$     | Ich habe 50% mehr Zeit als gefordert aufgewendet     |
| $0,5$     | Ich habe halb so viel Zeit wie gefordert aufgewendet |

# Die Entdeckungen
Nach dieser Verarbeitung konnte ich endlich einen Blick darauf werfen wie viel Zeit ich pro Vorlesung aufgewendet hatte.
Das Erste, was ich mir anschaute, war der Durchschnitt des Zeitanteils $\lambda$ über alle Vorlesungen.
Dieser war $0,387$, was bedeutet, dass ich im Durchschnitt nicht einmal 40% der geforderten Zeit pro Vorlesung aufgewendet habe.
Eine weitere Idee, welcher ich nachgehen wollte, war zu sehen, ob ich einen Unterschied in den Daten zwischen den drei Universitäten, die ich besucht habe, finden konnte.
Diese drei Universitäten sind die Hochschule Reutlingen, die Eberhard Karls Universität in Tübingen und die Universität Helsinki.
Daher habe ich als Nächstes die Durchschnitte für die jeweiligen Universitäten verglichen.
Dies führte zu den folgenden Ergebnissen:

| University | $\bar{\lambda}$ |
| ---------- | --------------- |
| Reutlingen | $0,400$         |
| Tübingen   | $0,481$         |
| Helsinki   | $0,340$         |

Ich hatte erwartet, dass die aufgewendete Zeit in Tübingen höher als für die anderen Universitäten liegen würde, was mich überraschte war, dass der Zeitanteil für Helsinki so viel geringer als für Reutlingen war.
Jedoch habe ich im Buch [Factfulness]({% post_url de/2020-10-24-factfulness %}) gelernt, dass Durchschnitte trügen können, daher habe ich mit einer grafischen Analyse weiter in die Daten hinein geschaut.
Hierfür habe ich die [Plotly Bibliothek](https://plotly.com/python/) verwendet, um interaktive Graphen zu generieren.

Der folgende Graph, bei welchem ich schließlich ankam, erfordert eventuell ein wenig Erklärung:
Diese Art von Graph wird [Box-Plot](https://de.wikipedia.org/wiki/Box-Plot) genannt, mit dem Unterschied, in meinem Plot zusätzlich die einzelnen Datenpunkte als Markierungen eingezeichnet sind.
Mithilfe eines Box-Plots kann die Verteilung einer Gruppe von Datenpunkten dargestellt werden.
Dies beinhaltet den maximalen und minimalen Wert, abgesehen von Ausreißern (dargestellt durch den oberen und unteren horizontalen Strich), die Box repräsentiert den Bereich in welchem die mittleren 50% der Datenpunkte liegen und die Linie in der Mitte stellt den Median der Daten dar.
Durch diese Art der Darstellung kann viel Information über die Verteilung verhältnismäßig einfach dargestellt werden.
Zusätzlich habe ich mich dazu entschieden alle Datenpunkte einzeln darzustellen.
Dies erlaubt es interaktiv einzelne Vorlesungen zu betrachten, die exakten Werte abzulesen und potenziell Erklärungen für Ausreißer zu finden.

{% include specials/university_timelogs_plot.html %}

Zuerst hatte ich diesen Graphen ohne die Farbkategorie, welche darstellt, ob eine Vorlesung ein online Kurs war oder nicht, erstellt.
Als ich diesen Graphen betrachtet habe fiel mir auf, dass drei Vorlesungen in Helsinki einen nennenswert niedrigeren Wert hatte als die anderen.
Bei genauerer Betrachtung stellte sich heraus, dass diese drei Kurse online Kurse waren, die aus reinem Selbststudium basierend auf online Materialien bestanden.
Daher habe ich mich entschieden, diese Kurse in ihre eigene Gruppe zu separieren, um die Daten der 'normalen' Vorlesungen in Helsinki nicht zu verzerren.
Diese drei Kurse erklären auch warum der Durchschnittswert für Helsinki so überraschend tief lag.

Einige interessante Dinge welche mir in diesem Graphen aufgefallen sind:
* Nur ein einziger Kurs kommt dem nahe, dass ich die geforderte Zeit für den Kurs wirklich aufgewendet hätte.
  Dieser Kurs ist meine Bachelorarbeit, welche ich in Reutlingen geschrieben habe.
* Die Daten für Reutlingen sind recht weit verteilt, was darauf hindeutet, dass die Vorlesungen dort sehr ungleich bezüglich des Arbeitsaufwandes sind.
* Online Kurse scheinen ein zeiteffizienter Weg, um Credits zu bekommen, zu sein.
* Mein durchschnittlicher Zeitaufwand für Kurse in Tübingen war der höchste, jedoch habe ich dort auch nur drei Vorlesungen besucht was nicht so viele Daten liefert.

# Was nun?
Und was tun wir jetzt mit diesen Entdeckungen?
Um ehrlich zu sein, ich weiß es auch nicht.
Eine Sache, welche ich realisiert habe, ist, dass ein Bauchgefühl darüber, wie viel Zeit man für etwas aufwendet, nicht unbedingt ein guter Indikator ist.
Die Daten zeigen zwar, dass ich für die Vorlesungen in Tübingen mehr Zeit aufgewendet habe, allerdings hatte ich erwartet, dass dieser Unterschied deutlich größer ausfallen würde.
Das andere Bauchgefühl, dass ich im Allgemeinen weniger Zeit für Vorlesungen aufwende als gefordert, hat sich jedoch bestätigt.

Eine letzte Randbemerkung zu den Daten, welche ich aufgezeichnet habe.
Dies sind offensichtlich nur meine persönlichen Erfahrungen.
Es wäre sehr interessant diese Entdeckungen mit Daten von anderen Studierenden zu vergleichen um zu sehen wie diese abweichen.
Zusätzlich, auch wenn ich sagen würde, dass ich sehr gewissenhaft mit meiner Datenerfassung bin, ist mir bewusst, dass ich immer wieder Ideen wie eine Aufgabe gelöst werden kann oder wie ich ein Problem in dem ich gerade stecke umgehen kann, während ich eine Pause mache.
Diese Zeiten werden offensichtlich nicht erfasst.
Daher kann der gefundene Zeitanteil vielleicht eher als eine Untergrenze betrachtet werden.
Dennoch würde ich sagen, dass die 25 Stunden pro Credit nicht auf meine Arbeitsweise für die Uni zutreffen.