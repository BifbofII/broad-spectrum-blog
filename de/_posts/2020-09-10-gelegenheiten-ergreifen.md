---
title: "Gelegenheiten ergeifen"
ref: seizing-opportunities
category: personal
verse: Solange wir also noch Gelegenheit dazu haben, wollen wir allen Menschen Gutes tun, ganz besonders denen, die wie wir durch den Glauben zur Familie Gottes gehören. --- **Galater 6,10**
header:
  overlay_image: assets/images/headers/opportunity.jpg
  overlay_filter: .5
  show_overlay_excerpt: false
  image_description: Eine offene Tür
---
Eine Sache die ich mir in letzter Zeit mehr ins Bewusstsein rufe ist, wie ich mit auftretenden Gelegenheiten umgehe.
Früher habe ich Entscheidungen über Gelegenheiten hauptsächlich aus dem Bauch heraus getroffen.
Das bedeutete, wenn ich momentan müde war, war es unwahrscheinlich, dass ich mich auf Pläne einlassen würde, selbst wenn sie für den nächsten Tag waren.

# Das Problem
In meiner aktuellen Situation, das heißt in einer Stadt, die mir noch neu ist und in der ich kaum Leute kenne, könnte dieses Verhalten dazu führen, dass ich die meiste Zeit zu Hause verbringe.
Der Fakt, dass meine Vorlesungen online stattfinden macht es nicht einfacher die Gewohnheit der selbst verursachten Isolation zu durchbrechen.

Neue Leute kennenzulernen ist in dieser Situation schwieriger als es im Normalfall zu Beginn eines Studiums an einer neuen Universität wäre.
In großen Online-Vorlesungen sieht man von den anderen Studenten nicht mehr als einen Namen auf dem Bildschirm.
Selbst wenn es Kommilitonen gibt welche dieselben Kurse wie ich belegen ist es schwierig diese zu finden, da man sich nicht in den Hörsälen über den Weg läuft.
Sicher, es gibt Gruppenchats und kleinere Übungsgruppen, in denen man mit Kommilitonen sprechen und in Kleingruppen Aufgaben bearbeiten kann, aber das ist sicherlich nicht dasselbe wie es unter normalen Umständen wäre.

# Die Entscheidung
Die Frage ist nun, wie ich mit dieser Situation umgehen möchte.
Sage ich zu mir selbst: "Ach, die Umstände werden sich schon wieder verändern, ich kann auch nächstes Semester noch neue Leute kennenlernen, wenn Vorlesungen wieder am Campus stattfinden." oder entscheide ich mich etwas zu unternehmen und das beste daraus zu machen?

Wozu ich mich entschieden habe ist, meine Chancen interessante Leute kennenzulernen zu erhöhen, indem ich nahezu alle Gelegenheiten ergreife, die auf mich zukommen.
Ich habe schon die meisten Treffen für neue Studenten während der Einführungswoche besucht, versuche die wenigen Offline-Veranstaltungen, die mich interessieren zu finden und dort Leute kennenzulernen.
Zusätzlich besuche ich Online-Lerngruppen, um dort Kommilitonen aus meinen Kursen kennenzulernen, ich war bereits in einer Gemeinde und möchte bald noch eine andere besuchen, um nur ein paar Dinge zu nennen.

Nachdem ich diese, grundsätzlich akzeptierende Haltung nun bereits eine Weile ausprobiert habe, durfte ich schon ein paar Erfahrungen machen, die mich positiv überrascht haben.
Ich würde mich selbst eher als eine introvertierte Person beschreiben, somit ist es für mich nicht einfach irgendwo hinzugehen, wo ich niemanden kenne und neue Leute kennenzulernen.
Aber schon aus meiner wenigen Erfahrung heraus kann ich sagen, dass die Angst vor schlechten Erfahrungen irrational ist.
Natürlich waren nicht alle Veranstaltungen, bei denen ich war ein voller Erfolg, aber andere Leute sind grundsätzlich freundlich zu Neuankömmlingen, im Speziellen bei Veranstaltungen zu Beginn des neuen akademischen Jahres und, wie bereits erwähnt haben manche Situationen zu erfreulichen Überraschungen geführt.

Das Gute an dieser Herangehensweise ist, dass sich mehr Gelegenheiten ergeben, wenn man mehr Gelegenheiten ergreift.
Selbst wenn die ersten paar nicht genau das Erhoffte sind, eventuell ergeben sich daraus die Gelegenheiten, auf die man schon länger wartet.

# Gelegenheiten die uns geschenkt werden
Ein letzter Gedanke zu diesem Thema.
Ich denke, dass sich Gelegenheiten nicht aus purem Glück ergeben, sondern, dass sie uns geschenkt werden.
Als Christ denke ich, dass Gott für uns sorgt und das Beste für uns möchte.
Aus diesem Grund bin ich mir sicher, dass er uns Gelegenheiten schenkt und wir diese nur ergreifen müssen.
Es gibt viele Geschichten in der Bibel in denen Gott Menschen mit Gelegenheiten versorgt, aber es ist ihre Aufgabe diese umzusetzen.
Nehmen wir Nehemia zum Beispiel.
Als Mundschenk des Königs hatte Gott ihn mit der Gelegenheit versorgt den König darum zu beten, die Mauer um Jerusalem wieder aufbauen zu dürfen, aber er selbst musste diese Gelegenheit ergreifen und wirklich zum König sprechen.

Um keine gute Gelegenheit die mir geschenkt wird zu verpassen, möchte ich mir bewusster Gedanken machen welche Entscheidungen ich treffe.