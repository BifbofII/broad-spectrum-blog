---
layout: single
classes: wide
title: Über diesen Blog
permalink: de/about
ref: about
author_profile: true
header:
  overlay_image: assets/images/headers/main-header.jpg
  image_description: "Helsinki Cathedral"
---

Hi, ich bin Christoph, Informatikstudent aus dem Süden Deutschlands und lebe in Helsinki, Finnland.
Als ich nach Finnland gezogen bin stellte ich fest, dass mir vermutlich einiges durch den Kopf gehen würde und um den Beginn eines neuen Kapitels meines Lebens zu dokumentieren und die Gedanken in meinem Kopf zu strukturieren kam die Idee auf einen Blog darüber zu schreiben.

Jedoch möchte ich diesen Blog nichtauf das Thema des Umzuges in ein neues Land beschränken.
Ich denke über sehr viele unterschiedliche Dinge nach und möchte mir die Freiheit erhalten über lle diese Dinge in diesem Blog zu schreiben.
Daher kam auch der Name des Blogs, du kannst ein _breites Spektrum_ an Inhalten in den Posts erwarten.

Nun ein bisschen mehr zu mir.
Wie bereits erwähnt bin ich aus dem Südwesten Deutschlands und nachdem ich mein Bachelorstudium in Mechatronik dort abgeschlossen hatte habe ich mich entschieden, dass ich für mein Masterstudium etwas anderes ausprobieren wollte.
Dabei kam die Idee auf nach Nordeuropa zu ziehen und dort zu studieren.
Schließlich hatte ich einen Studienplatz an der Universität Helsinki im Informatik Master Programm.

Neben meinem Studium interessiere ich mich für Musik, ein bisschen Photografie, Klettern und ich bin Christ, das heißt aus diesen Bereichen kannst du vermutlich Posts erwarten.
Fühl dich frei mich bei Fragen zu kontaktieren.