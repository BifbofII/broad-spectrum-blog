#!/usr/bin/bash
folder=$1
name_en=$2
name_de=$3

gallery_nr=$(basename $folder)

# Copy original files to external host
rsync -r --chmod=o+r,Do+x $folder jabsserver.net:/tank/services/webserver/gallery

# Add to english gallery
gawk -i inplace "/# <insert-mark-1>/ { print \"gallery-${gallery_nr}:\"; print; next }1" en/gallery.md
for img in $folder/*
do
  gawk -i inplace "/# <insert-mark-1>/ { print \"  - url: https://www.jabsserver.net/gallery/${gallery_nr}/$(basename $img)\n    image_path: ${img%.*}-th.jpg\"; print; next }1" en/gallery.md
done
gawk -i inplace "/(insert-mark-2)/ { print; print \"{% include gallery id=\\\"gallery-${gallery_nr}\\\" caption=\\\"${name_en}\\\" %}\"; next }1" en/gallery.md

# Add to german gallery
gawk -i inplace "/# <insert-mark-1>/ { print \"gallery-${gallery_nr}:\"; print; next }1" de/gallery.md
for img in $folder/*
do
  gawk -i inplace "/# <insert-mark-1>/ { print \"  - url: https://www.jabsserver.net/gallery/${gallery_nr}/$(basename $img)\n    image_path: ${img%.*}-th.jpg\"; print; next }1" de/gallery.md
done
gawk -i inplace "/(insert-mark-2)/ { print; print \"{% include gallery id=\\\"gallery-${gallery_nr}\\\" caption=\\\"${name_de}\\\" %}\"; next }1" de/gallery.md

originals=$(ls $folder)
# Generate thumbnails
helpers/mkthmbs.sh "$folder/*"
# Remove original files
for org in $originals
do
  rm $folder/$org
done
