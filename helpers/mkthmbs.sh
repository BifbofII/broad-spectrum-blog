#!/usr/bin/bash
for f in $@
do
  convert $f -resize 640 "${f%.*}"-th.jpg
done
