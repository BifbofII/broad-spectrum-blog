---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: splash
author_profile: true
title: Broad Spectrum Blog
locale: en-GB
entries_layout: grid
header:
  overlay_image: assets/images/headers/main-header.jpg
  image_description: "Helsinki Cathedral"
excerpt: "Welcome to my Blog - Willkommen auf meinem Blog"
intro: 
  - excerpt: 'Welcome to my blog, please select you language - Willkommen auf meinem Blog, bitte wähle deine Sprache aus.'
language_row:
  - image_path: assets/images/british-flag.png
    alt: "British Flag"
    title: "English"
    url: "en"
    btn_label: "Select"
    btn_class: "btn--primary"
  - image_path: assets/images/german-flag.png
    alt: "German Flag"
    title: "Deutsch"
    url: "de"
    btn_label: "Auswählen"
    btn_class: "btn--primary"
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="language_row" %}